package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Article;

@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
   @Update("UPDATE t_article SET is_recommend=#{recommend},create_time=#{time} WHERE id=#{id}")
   public int recommend(@Param("recommend")Integer recommend,@Param("time")String time,@Param("id")int id);
   
   @Select("SELECT a.*,ac.`name` FROM t_article as a,t_article_type as ac WHERE a.type_id=ac.id ${sql} ORDER BY is_recommend desc,create_time DESC LIMIT #{index},#{size}")
   public List<Map<String, Object>> getList(@Param("sql")String sql,@Param("index")int index,@Param("size")int size);
   
   @Select("SELECT ifnull(count(1),0) FROM t_article as a,t_article_type as ac WHERE a.type_id=ac.id ${sql}")
   public int getTotalCount(@Param("sql")String sql);
   
   @Select("SELECT a.* FROM t_user_collect as u LEFT JOIN t_article as a ON u.article_id=a.id WHERE u.user_id=#{userId} LIMIT #{pageIndex},10")
   public List<Article> selectByUserId(@Param("userId")int userId,@Param("pageIndex")int pageIndex);
}