package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.DoctorWork;

public interface DoctorWorkMapper extends BaseMapper<DoctorWork> {
	@Update("UPDATE t_doctor_work SET doctor_id=#{did} WHERE id=#{id}")
	public int editDoctorId(@Param("did")int did,@Param("id")int id);
}