package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.InsuranceDetail;

public interface InsuranceDetailMapper extends BaseMapper<InsuranceDetail> {
	int deleteByPrimaryKey(Integer id);

	int insertSelective(InsuranceDetail record);

	InsuranceDetail selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(InsuranceDetail record);

	int updateByPrimaryKeyWithBLOBs(InsuranceDetail record);

	int updateByPrimaryKey(InsuranceDetail record);
}