package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.UserReceive;

public interface UserReceiveMapper extends BaseMapper<UserReceive> {
    int deleteByPrimaryKey(Integer id);


    int insertSelective(UserReceive record);

    UserReceive selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserReceive record);

    int updateByPrimaryKey(UserReceive record);
    
    @Select("SELECT u.*,r.create_time FROM t_user_receive as r LEFT JOIN t_user as u ON r.user_id=u.id WHERE u.`name` LIKE #{name}  LIMIT #{index},10")
    public List<Map<String, Object>> getList(@Param("name")String name,@Param("index")Integer index);
    
    @Select("SELECT count(1) FROM t_user_receive as r LEFT JOIN t_user as u ON r.user_id=u.id WHERE u.`name` LIKE #{name} ")
    public int getListCount(@Param("name")String name);
}