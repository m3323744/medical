package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.UserIllness;

public interface UserIllnessMapper extends BaseMapper<UserIllness> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(UserIllness record);

    UserIllness selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserIllness record);

    int updateByPrimaryKey(UserIllness record);
    
    @Select("SELECT u.*,i.`name` FROM t_user_illness as u LEFT JOIN t_illness_type as i ON i.id=u.type_id WHERE u.user_id=#{userId} limit #{pageIndex},10")
    public List<Map<String, Object>> selectByUserId(@Param("userId")int userId,@Param("pageIndex")int pageIndex);
}