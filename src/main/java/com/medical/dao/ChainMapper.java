package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Chain;

public interface ChainMapper extends BaseMapper<Chain> {
	int deleteByPrimaryKey(Integer id);

	int insertSelective(Chain record);

	Chain selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Chain record);

	int updateByPrimaryKeyWithBLOBs(Chain record);

	int updateByPrimaryKey(Chain record);
}