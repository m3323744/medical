package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.ArticleType;

public interface ArticleTypeMapper extends BaseMapper<ArticleType> {
    int deleteByPrimaryKey(Integer id);


    int insertSelective(ArticleType record);

    ArticleType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ArticleType record);

    int updateByPrimaryKey(ArticleType record);
}