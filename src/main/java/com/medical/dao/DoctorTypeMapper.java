package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.DoctorType;

public interface DoctorTypeMapper extends BaseMapper<DoctorType> {
    int deleteByPrimaryKey(Integer id);


    int insertSelective(DoctorType record);

    DoctorType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DoctorType record);

    int updateByPrimaryKey(DoctorType record);
    
    @Select("SELECT * FROM t_doctor_type WHERE `name`=#{type} LIMIT 1")
    DoctorType selectByName(@Param("type")String type);
}