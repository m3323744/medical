package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.ServiceCenter;

public interface ServiceCenterMapper extends BaseMapper<ServiceCenter> {
	int deleteByPrimaryKey(Integer id);

	int insertSelective(ServiceCenter record);

	ServiceCenter selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ServiceCenter record);

	int updateByPrimaryKeyWithBLOBs(ServiceCenter record);

	int updateByPrimaryKey(ServiceCenter record);

	List<Map<String, Object>> getListByPage(@Param("name") String name, @Param("areaId") int areaId, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getListCount(@Param("name") String name, @Param("areaId") int areaId);
}