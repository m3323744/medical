package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Instructor;

public interface InstructorMapper extends BaseMapper<Instructor> {
    int deleteByPrimaryKey(Integer id);


    int insertSelective(Instructor record);

    Instructor selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Instructor record);

    int updateByPrimaryKey(Instructor record);
}