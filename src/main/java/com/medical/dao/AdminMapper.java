package com.medical.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Admin;

@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
	@Select("SELECT * FROM t_admin WHERE username=#{username} AND `password`=#{pwd}")
	public Admin getByUser(@Param("username")String username,@Param("pwd")String pwd);
}
