package com.medical.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Insurance;


public interface InsuranceMapper extends BaseMapper<Insurance> {

}