package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Doctor;

public interface DoctorMapper extends BaseMapper<Doctor> {
	int deleteByPrimaryKey(Integer id);

	int insertSelective(Doctor record);

	Doctor selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Doctor record);

	int updateByPrimaryKey(Doctor record);

	@Update("UPDATE t_doctor SET `status`=#{status} WHERE id=#{id}")
	public int updateStates(@Param("status") int status, @Param("id") int id);

	List<Map<String, Object>> getListByPage(@Param("name") String name, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getListCount(@Param("name") String name);

	@Select("SELECT COUNT(1) FROM t_doctor WHERE user_id=#{userId}")
	public int isExist(@Param("userId") int userId);

	@Select("SELECT * FROM t_doctor WHERE user_id=#{userId}")
	public Doctor isExistDoc(@Param("userId") int userId);

	List<Map<String, Object>> getDoctorListByPage(@Param("name") String name, @Param("illnessTypeId") int illnessTypeId, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getDoctorListCount(@Param("name") String name, @Param("illnessTypeId") int illnessTypeId);

	Map<String, Object> getDetail(@Param("id") int id);

}