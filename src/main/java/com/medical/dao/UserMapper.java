package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.User;

public interface UserMapper extends BaseMapper<User> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    @Update("UPDATE t_user SET is_del=#{isDel} WHERE id=#{id}")
    public int deleteUser(@Param("isDel")Boolean isDel,@Param("id")int id);
    
    @Update("UPDATE t_user SET `name`=#{name},head_url=#{headurl} WHERE id=#{id}")
    public int updateName(@Param("name")String name,@Param("headurl")String headurl,@Param("id")int id);
    
    @Update("UPDATE t_user SET mobile=#{phone} WHERE id=#{id}")
    public int updateMobile(@Param("phone")String phone,@Param("id")int id);
}