package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Order;

public interface OrderMapper extends BaseMapper<Order> {
	int deleteByPrimaryKey(Integer id);

	int insertSelective(Order record);

	Order selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Order record);

	int updateByPrimaryKey(Order record);

	List<Map<String, Object>> getListByPage(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getListCount(@Param("beginTime") String beginTime, @Param("endTime") String endTime);
	
	@Select("SELECT o.id,o.order_no,o.amounts,o.create_time,s.img_url,s.`name`,s.latitude,s.longitude FROM t_order as o LEFT JOIN t_instructor as i ON o.instructor_id=i.id LEFT JOIN t_service_center as s ON i.service_id=s.id WHERE o.user_id=#{userId} limit #{pageIndex},10")
	public List<Map<String, Object>> getByUserId(@Param("userId")int userId,@Param("pageIndex")int pageIndex);
}