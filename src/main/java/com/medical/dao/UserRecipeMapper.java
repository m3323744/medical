package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.UserRecipe;

public interface UserRecipeMapper extends BaseMapper<UserRecipe> {
	int deleteByPrimaryKey(Integer id);

	int insertSelective(UserRecipe record);

	UserRecipe selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(UserRecipe record);

	int updateByPrimaryKey(UserRecipe record);

	List<Map<String, Object>> getListByPage(@Param("userId") int userId, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getListCount(@Param("userId") int userId);

}