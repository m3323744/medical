package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.IllnessType;

public interface IllnessTypeMapper extends BaseMapper<IllnessType> {
    int deleteByPrimaryKey(Integer id);


    int insertSelective(IllnessType record);

    IllnessType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IllnessType record);

    int updateByPrimaryKey(IllnessType record);
    
    @Select("SELECT * FROM t_illness_type WHERE `name`=#{name} LIMIT 1")
    public IllnessType selectByName(@Param("name")String name);
}