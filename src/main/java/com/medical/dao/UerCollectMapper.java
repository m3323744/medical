package com.medical.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.UerCollect;

public interface UerCollectMapper extends BaseMapper<UerCollect>{
    int deleteByPrimaryKey(Integer id);

    int insertSelective(UerCollect record);

    UerCollect selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UerCollect record);

    int updateByPrimaryKey(UerCollect record);
    
    @Select("SELECT * FROM t_user_collect WHERE user_id=#{coll.userId} AND article_id=#{coll.articleId}")
    public UerCollect checkCollect(@Param("coll")UerCollect coll);
}