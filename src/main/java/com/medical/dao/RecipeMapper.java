package com.medical.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.medical.entity.Recipe;

public interface RecipeMapper extends BaseMapper<Recipe> {
	int deleteByPrimaryKey(Integer id);

	Integer insert(Recipe record);

	int insertSelective(Recipe record);

	Recipe selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Recipe record);

	int updateByPrimaryKey(Recipe record);

	List<Map<String, Object>> getListByPage(@Param("name") String name, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getListCount(@Param("name") String name);

	List<Map<String, Object>> getListPage(@Param("illnessTypeId") Integer illnessTypeId, @Param("name") String name, @Param("beginIndex") int beginIndex, @Param("size") int size);

	int getListPageCount(@Param("illnessTypeId") Integer illnessTypeId, @Param("name") String name);
}