package com.medical.entity;

public class Markers {
	private int id;
	private Double latitude;
	private Double longitude;
	private String title;
	private String iconPath="../../images/dingwei_icon.png";
	private int width=25;
	private int height=25;
	private Callout callout;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIconPath() {
		return iconPath;
	}
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public Callout getCallout() {
		return callout;
	}
	public void setCallout(Callout callout) {
		this.callout = callout;
	}
	
}
