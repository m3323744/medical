package com.medical.entity;

public class Callout {
	private String content;
	private int padding=8;
	private String display="BYCLICK";
	private int borderRadius=4;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getPadding() {
		return padding;
	}
	public void setPadding(int padding) {
		this.padding = padding;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public int getBorderRadius() {
		return borderRadius;
	}
	public void setBorderRadius(int borderRadius) {
		this.borderRadius = borderRadius;
	}
	
}
