package com.medical.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("t_recipe")
public class Recipe extends Model<Recipe> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5965495541815133727L;

	@TableId
	private Integer id;

	private String name;

	private Integer doctorId;

	private Integer illnessTypeId;

	private String imgUrl;

	private Float exFee;

	private Float price;

	private String unit;

	private String rate;

	private String strength;

	private String heartRate;

	private Integer isInsurance;

	private Integer status;

	private String content;

	private Date createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public Integer getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	public Integer getIllnessTypeId() {
		return illnessTypeId;
	}

	public void setIllnessTypeId(Integer illnessTypeId) {
		this.illnessTypeId = illnessTypeId;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl == null ? null : imgUrl.trim();
	}

	public Float getExFee() {
		return exFee;
	}

	public void setExFee(Float exFee) {
		this.exFee = exFee;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit == null ? null : unit.trim();
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate == null ? null : rate.trim();
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength == null ? null : strength.trim();
	}

	public String getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(String heartRate) {
		this.heartRate = heartRate == null ? null : heartRate.trim();
	}

	public Integer getIsInsurance() {
		return isInsurance;
	}

	public void setIsInsurance(Integer isInsurance) {
		this.isInsurance = isInsurance;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}