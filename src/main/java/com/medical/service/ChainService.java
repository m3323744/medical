package com.medical.service;

import java.util.Map;

import com.medical.entity.Chain;
import com.medical.entity.ResultBean;

public interface ChainService {
	public Map<String, Object> query(int pageIndex, Map<String, Object> map);

	public Map<String, Object> toAdd(Map<String, Object> map);

	public int add(Chain chain, String path);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(Chain chain, String path);

	public int delete(int id);

	public ResultBean<Chain> getDetail();

}
