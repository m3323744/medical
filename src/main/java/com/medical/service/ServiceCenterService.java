package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.ServiceCenter;

public interface ServiceCenterService {
	public Map<String, Object> query(String name, int areaId, int pageIndex, Map<String, Object> map);

	public Map<String, Object> toAdd(Map<String, Object> map);

	public int add(ServiceCenter serviceCenter, String path, String[] instructors, String[] levels);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(ServiceCenter serviceCenter, String path, String[] instructors, String[] levels);

	public int delete(int id);

	public int updateStatus(int id, int isTop);

	public ResultBean<Map<String, Object>> getList(Double latitude, Double longitude, String name);

}
