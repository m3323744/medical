package com.medical.service;

import java.util.Map;

import com.medical.entity.InsuranceDetail;
import com.medical.entity.ResultBean;

public interface InsuranceDetailService {
	public Map<String, Object> query(int type, int pageIndex, Map<String, Object> map);
	
	public Map<String, Object> toAdd(int type, Map<String, Object> map);

	public int add(InsuranceDetail insuranceDetail);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(InsuranceDetail insuranceDetail);

	public int delete(int id);

	public ResultBean<InsuranceDetail> getDetail(int type);

}
