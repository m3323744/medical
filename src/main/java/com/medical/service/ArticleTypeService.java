package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ArticleType;

public interface ArticleTypeService {
	public Map<String, Object>  getList(int pageIndex,Map<String, Object> map);
	public int delArtType(int id);
	public ArticleType getDateils(int id);
	public int updateArtType(ArticleType art);
	public int addUser(ArticleType art);
	public Boolean detectionType(String type);
	public List<ArticleType> getAll();
}
