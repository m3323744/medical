package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Img;
import com.medical.entity.ResultBean;

public interface ImgService {
	public Map<String, Object> query(int pageIndex, Map<String, Object> map);

	public int add(Img img, String path);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(Img img, String path);

	public int delete(int id);

	public int updateStatus(int id, int isShow);

	public ResultBean<List<Img>> getList();

}
