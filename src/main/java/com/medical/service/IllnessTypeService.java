package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.IllnessType;
import com.medical.entity.ResultBean;

public interface IllnessTypeService {
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map);

	public int update(IllnessType illnessType);

	public int add(IllnessType illnessType);

	public int delete(int id);
	
	public ResultBean<List<IllnessType>> getType();

}
