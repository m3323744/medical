package com.medical.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Article;
import com.medical.entity.ArticleType;
import com.medical.entity.ResultBean;

public interface ArticleService {
	public Map<String, Object> getList(int typeId, int pageIndex, Map<String, Object> map);

	public int delArtcle(int id);

	public Article getDateils(int id);

	public int updateArticle(Article art, MultipartFile file, HttpServletRequest request);

	public int addArticle(Article art, MultipartFile file, HttpServletRequest request);

	public int recommend(int id, int recommend);

	public ResultBean<List<ArticleType>> getType();

	public ResultBean<List<Article>> getByKeyWord(String keyWord);
}
