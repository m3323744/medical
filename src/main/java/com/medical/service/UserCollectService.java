package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.UerCollect;

public interface UserCollectService {
	public ResultBean<Integer> addCollect(UerCollect coll);
	public ResultBean<Integer> checkCollect(UerCollect coll);
	public ResultBean<Map<String, Object>> getList(int userId,int pageIndex);
}
