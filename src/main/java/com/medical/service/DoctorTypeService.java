package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.DoctorType;
import com.medical.entity.ResultBean;

public interface DoctorTypeService {
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(DoctorType doctorType);

	public int add(DoctorType doctorType);

	public int delete(int id);
	
	public ResultBean<List<DoctorType>> getAll();
}
