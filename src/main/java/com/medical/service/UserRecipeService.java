package com.medical.service;

import java.util.Map;

public interface UserRecipeService {
	public Map<String, Object> getList(int userId, int pageIndex, Map<String, Object> map);

	public int getCount(int userId);
}
