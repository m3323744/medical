package com.medical.service;

import com.medical.entity.Admin;

public interface AdminService {
	public Admin getByUser(String username,String pwd);
	public int editPwd(Admin ad);
}
