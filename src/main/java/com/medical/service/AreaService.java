package com.medical.service;

import java.util.Map;

import com.medical.entity.Area;

public interface AreaService {
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(Area area);

	public int add(Area area);

	public int delete(int id);

}
