package com.medical.service;

import java.util.Map;

import com.medical.entity.Recipe;
import com.medical.entity.ResultBean;

public interface RecipeService {
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map);

	public Map<String, Object> toAdd(Map<String, Object> map);

	public int add(Recipe recipe, String path);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(Recipe recipe, String path);

	public int delete(int id);

	public Map<String, Object> getList(int pageIndex, Integer illnessTypeId, String name, Map<String, Object> map);

	public ResultBean<Map<String, Object>> getDetail(int id);

}
