package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.UserRecipeMapper;
import com.medical.service.UserRecipeService;

@Service
@Transactional
public class UserRecipeServiceImpl implements UserRecipeService {
	Logger logger = LoggerFactory.getLogger(UserRecipeServiceImpl.class);

	@Resource
	private UserRecipeMapper userRecipeMapper;

	@Override
	public Map<String, Object> getList(int userId, int pageIndex, Map<String, Object> map) {
		try {
			int size = 10;
			List<Map<String, Object>> data = userRecipeMapper.getListByPage(userId, (pageIndex - 1) * size, size);
			int total = userRecipeMapper.getListCount(userId);
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages = (total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info("前端处方列表查询：" + e.getMessage());
			return map;
		}
	}

	@Override
	public int getCount(int userId) {
		try {
			int count = userRecipeMapper.getListCount(userId);
			return count;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info("前端处方列表查询：" + e.getMessage());
			return 0;
		}
	}

}
