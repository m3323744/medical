package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.InsuranceMapper;
import com.medical.entity.Insurance;
import com.medical.entity.ResultBean;
import com.medical.service.InsuranceService;

@Service
@Transactional
public class InsuranceServiceImpl implements InsuranceService {
	Logger logger = LoggerFactory.getLogger(InsuranceServiceImpl.class);

	@Resource
	private InsuranceMapper insuranceMapper;

	@Override
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<Insurance> data = insuranceMapper.selectPage(new Page<Insurance>(pageIndex, size), new EntityWrapper<Insurance>().like("name", name).orderBy("id", false));
		// 获取总条数，并释放资源
		int total = insuranceMapper.selectCount(new EntityWrapper<Insurance>().like("name", name));
		map.put("name", name);
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public int add(Insurance insurance, String titleUrl, String path) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<Insurance> list = insuranceMapper.selectList(new EntityWrapper<Insurance>().eq("name", insurance.getName()));
			if (list != null && list.size() > 0) {
				count = -1;
			}
			if (list == null || list.size() == 0) {
				insurance.setTitleUrl(titleUrl);
				insurance.setImgUrl(path);
				count = insuranceMapper.insert(insurance);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Insurance insurance = insuranceMapper.selectById(id);
			map.put("insurance", insurance);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(Insurance insurance, String titleUrl, String path) {
		// TODO Auto-generated method stub
		try {
			if (!("").equals(titleUrl)) {
				insurance.setTitleUrl(titleUrl);
			}
			if (!("").equals(path)) {
				insurance.setImgUrl(path);
			}
			int count = insuranceMapper.updateById(insurance);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			int count = insuranceMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> getList(int pageIndex,Map<String, Object> map) {
		try {
			int size = 10;
			List<Insurance> data = insuranceMapper.selectPage(new Page<Insurance>(pageIndex, size), new EntityWrapper<Insurance>().orderBy("id", false));
			// 获取总条数，并释放资源
			int total = insuranceMapper.selectCount(new EntityWrapper<Insurance>());
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages = (total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info("前端保险列表查询：" + e.getMessage());
			return map;
		}
	}

	@Override
	public ResultBean<Insurance> getDetail(int id) {
		ResultBean<Insurance> bean = new ResultBean<Insurance>();
		try {
			Insurance Insurance = insuranceMapper.selectById(id);
			bean = new ResultBean<Insurance>(Insurance);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<Insurance>(e);
			logger.info("前端保险详情查询：" + e.getMessage());
			return bean;
		}
	}
}
