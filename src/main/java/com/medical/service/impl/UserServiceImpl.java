package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.UserMapper;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	Logger logger  =  LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Resource
	private UserMapper dao;
	
	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getList(String keyWord, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size=10;
		try {
			List<User> data=dao.selectPage(new Page<User>(pageIndex, size), new EntityWrapper<User>().eq("is_del", false).andNew("(name like '%"+keyWord+"%'").orNew("mobile like '%"+keyWord+"%')"));
			// 获取总条数，并释放资源
			int total = dao.selectCount(new EntityWrapper<User>().eq("is_del", false).andNew("(name like '%"+keyWord+"%'").orNew("mobile like '%"+keyWord+"%')"));
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages=(total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			map.put("keyWord", keyWord);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员列表查询："+e.getMessage());
			return null;
		}
	}

	@Override
	public int delUser(int id) {
		// TODO Auto-generated method stub
		try {
			return dao.deleteUser(true, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员删除："+e.getMessage());
			return 0;
		}
	}

	@Override
	public User getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			User user=dao.selectById(id);
			return user;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员详情查询："+e.getMessage());
			return null;
		}
		
	}

	@Override
	public int updateUser(User use) {
		// TODO Auto-generated method stub
		try {
			int count=dao.updateAllColumnById(use);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员详情更新："+e.getMessage());
			return 0;
		}
	}

	@Override
	public int addUser(User use) {
		// TODO Auto-generated method stub
		try {
			List<User> list=dao.selectList(new EntityWrapper<User>().eq("mobile", use.getMobile()));
			if(list.size()==0) {
				int count=dao.insert(use);
				return count;
			}else {
				return 0;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员添加："+e.getMessage());
			return 0;
		}
	}

	@Override
	public Boolean detectionUsername(String username) {
		// TODO Auto-generated method stub
		try {
			List<User> list=dao.selectList(new EntityWrapper<User>().eq("mobile", username));
			if(list.size()>0) {
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员添加帐号检索："+e.getMessage());
			return false;
		}
	}

	@Override
	public User addwxUser(User use) {
		// TODO Auto-generated method stub
		try {
			List<User> list=dao.selectList(new EntityWrapper<User>().eq("open_id", use.getOpenId()));
			if(list.size()==0&&use.getOpenId()!=null&&!use.getOpenId().equals("")) {
				dao.insert(use);
				return use;
			}else {
				return list.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("小程序端会员添加："+e.getMessage());
			return null;
		}
	}

	@Override
	public ResultBean<User> getApiDateils(int id) {
		// TODO Auto-generated method stub
		ResultBean<User> bean=null;
		try {
			User user=dao.selectById(id);
			bean=new ResultBean<>(user);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员详情查询："+e.getMessage());
			bean=new ResultBean<>(e);
		}
		return bean;
	}

	@Override
	public ResultBean<Integer> updateApiDateils(User use) {
		// TODO Auto-generated method stub
		ResultBean<Integer> bean=null;
		User user=dao.selectById(use.getId());
		user.setName(use.getName());
		user.setHeight(use.getHeight());
		user.setAge(use.getAge());
		user.setWeight(use.getWeight());
		user.setMobile(use.getMobile());
		user.setAddress(use.getAddress());
		user.setWeChat(use.getWeChat());
		user.setHeadUrl(use.getHeadUrl());
		try {
			int count=dao.updateAllColumnById(user);
			bean=new ResultBean<Integer>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<Integer>(e);
			logger.info("小程序个人信息编辑："+e.getMessage());
		}
		return bean;
	}

	@Override
	public ResultBean<Integer> updateName(String name, String headurl, int id) {
		// TODO Auto-generated method stub
		ResultBean<Integer> bean=null;
		User user=dao.selectById(id);
		try {
			if((user.getName()==null||user.getName().equals(""))&&(user.getHeadUrl()==null||user.getHeadUrl().equals(""))) {
				int count=dao.updateName(name, headurl, id);
				bean=new ResultBean<Integer>(count);
			}else {
				bean=new ResultBean<Integer>(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<Integer>(e);
			logger.info("小程序微信信息编辑："+e.getMessage());
		}
		return bean;
	}

	@Override
	public ResultBean<Integer> updatePhone(String phone, int id) {
		// TODO Auto-generated method stub
		ResultBean<Integer> bean=null;
		try {
			List<User> list=dao.selectList(new EntityWrapper<User>().eq("mobile", phone));
			if(!list.isEmpty()) {
				bean= new  ResultBean<Integer>(0);
			}else {
				int count=dao.updateMobile(phone, id);
				bean=new ResultBean<Integer>(count);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<Integer>(e);
			logger.info("小程序手机号编辑："+e.getMessage());
		}
		return bean;
	}

}
