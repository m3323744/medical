package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.AreaMapper;
import com.medical.dao.ServiceCenterMapper;
import com.medical.entity.Area;
import com.medical.entity.ServiceCenter;
import com.medical.service.AreaService;

@Service
@Transactional
public class AreaServiceImpl implements AreaService {
	Logger logger = LoggerFactory.getLogger(AreaServiceImpl.class);

	@Resource
	private AreaMapper areaMapper;
	@Resource
	private ServiceCenterMapper serviceCenterMapper;

	@Override
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<Area> data = areaMapper.selectPage(new Page<Area>(pageIndex, size), new EntityWrapper<Area>().like("name", name).orderBy("id", false));
		// 获取总条数，并释放资源
		int total = areaMapper.selectCount(new EntityWrapper<Area>().like("name", name));
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Area area = areaMapper.selectById(id);
			map.put("area", area);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区域详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(Area area) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<Area> list = areaMapper.selectList(new EntityWrapper<Area>().eq("name", area.getName()));
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() != area.getId()) {
					count--;
				}
			}
			if (count >= 0) {
				count = areaMapper.updateById(area);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区域更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			serviceCenterMapper.delete(new EntityWrapper<ServiceCenter>().eq("area_id", id));
			int count = areaMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区域删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int add(Area area) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<Area> list = areaMapper.selectList(new EntityWrapper<Area>().eq("name", area.getName()));
			if (list != null && list.size() > 0) {
				count = -1;
			}
			if (list == null || list.size() == 0) {
				count = areaMapper.insertSelective(area);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区域增加：" + e.getMessage());
			return 0;
		}
	}

}
