package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.ImgMapper;
import com.medical.entity.Img;
import com.medical.entity.ResultBean;
import com.medical.service.ImgService;

@Service
@Transactional
public class ImgServiceImpl implements ImgService {
	Logger logger = LoggerFactory.getLogger(ImgServiceImpl.class);

	@Resource
	private ImgMapper imgMapper;

	@Override
	public Map<String, Object> query(int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<Img> data = imgMapper.selectPage(new Page<Img>(pageIndex, size), new EntityWrapper<Img>().orderBy("id", false));
		// 获取总条数，并释放资源
		int total = imgMapper.selectCount(new EntityWrapper<Img>());
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public int add(Img img, String path) {
		// TODO Auto-generated method stub
		try {
			img.setImgUrl(path);
			img.setIsShow(1);
			int count = imgMapper.insertSelective(img);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("图片增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Img img = imgMapper.selectById(id);
			map.put("img", img);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("图片详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(Img img, String path) {
		// TODO Auto-generated method stub
		try {
			if (!("").equals(path)) {
				img.setImgUrl(path);
			}
			int count = imgMapper.updateById(img);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("图片更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			int count = imgMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("图片删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int updateStatus(int id, int isShow) {
		// TODO Auto-generated method stub
		try {
			Img img = new Img();
			img.setId(id);
			img.setIsShow(isShow);
			int count = imgMapper.updateById(img);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("图片显示隐藏：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<List<Img>> getList() {
		ResultBean<List<Img>> bean = new ResultBean<List<Img>>();
		try {
			List<Img> imgList = imgMapper.selectList(new EntityWrapper<Img>().eq("is_show", 1));
			bean = new ResultBean<List<Img>>(imgList);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<List<Img>>(e);
			logger.info("前端banner图列表查询：" + e.getMessage());
			return bean;
		}
	}
}
