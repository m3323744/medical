package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.IllnessTypeMapper;
import com.medical.dao.RecipeMapper;
import com.medical.entity.IllnessType;
import com.medical.entity.Recipe;
import com.medical.entity.ResultBean;
import com.medical.service.IllnessTypeService;

@Service
@Transactional
public class IllnessTypeServiceImpl implements IllnessTypeService {
	Logger logger = LoggerFactory.getLogger(IllnessTypeServiceImpl.class);

	@Resource
	private IllnessTypeMapper illnessTypeMapper;
	@Resource
	private RecipeMapper recipeMapper;

	@Override
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<IllnessType> data = illnessTypeMapper.selectPage(new Page<IllnessType>(pageIndex, size), new EntityWrapper<IllnessType>().like("name", name).orderBy("id", false));
		// 获取总条数，并释放资源
		int total = illnessTypeMapper.selectCount(new EntityWrapper<IllnessType>().like("name", name));
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public int update(IllnessType illnessType) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<IllnessType> list = illnessTypeMapper.selectList(new EntityWrapper<IllnessType>().eq("name", illnessType.getName()));
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() != illnessType.getId()) {
					count--;
				}
			}
			if (count >= 0) {
				count = illnessTypeMapper.updateById(illnessType);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方分类更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			recipeMapper.delete(new EntityWrapper<Recipe>().eq("illness_type_id", id));
			int count = illnessTypeMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方分类删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int add(IllnessType illnessType) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<IllnessType> list = illnessTypeMapper.selectList(new EntityWrapper<IllnessType>().eq("name", illnessType.getName()));
			if (list != null && list.size() > 0) {
				count = -1;
			}
			if (list == null || list.size() == 0) {
				count = illnessTypeMapper.insertSelective(illnessType);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方分类增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<List<IllnessType>> getType() {
		ResultBean<List<IllnessType>> bean = new ResultBean<List<IllnessType>>();
		try {
			List<IllnessType> typeList = illnessTypeMapper.selectList(new EntityWrapper<>());
			bean = new ResultBean<List<IllnessType>>(typeList);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<List<IllnessType>>(e);
			logger.info("前端处方分类列表查询：" + e.getMessage());
			return bean;
		}
	}

}
