package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.ChainMapper;
import com.medical.entity.Chain;
import com.medical.entity.ResultBean;
import com.medical.service.ChainService;

@Service
@Transactional
public class ChainServiceImpl implements ChainService {
	Logger logger = LoggerFactory.getLogger(ChainServiceImpl.class);

	@Resource
	private ChainMapper chainMapper;

	@Override
	public Map<String, Object> query(int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<Chain> data = chainMapper.selectPage(new Page<Chain>(pageIndex, size), new EntityWrapper<Chain>().orderBy("id", false));
		// 获取总条数，并释放资源
		int total = chainMapper.selectCount(new EntityWrapper<Chain>());

		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> toAdd(Map<String, Object> map) {

		// TODO Auto-generated method stub
		try {
			Chain chain = new Chain();
			List<Chain> list = chainMapper.selectList(new EntityWrapper<Chain>().orderBy("id", false));
			if (list != null && list.size() > 0) {
				chain = list.get(0);
			}
			map.put("chainDetail", chain);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区块链详情" + e.getMessage());
		}
		return map;
	}

	@Override
	public int add(Chain chain, String path) {
		// TODO Auto-generated method stub
		try {
			if (!("").equals(path)) {
				chain.setImgUrl(path);
			}
			int count = chainMapper.insert(chain);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区块链增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Chain chain = chainMapper.selectById(id);
			map.put("chain", chain);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区块链详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(Chain chain, String path) {
		// TODO Auto-generated method stub
		try {
			if (!("").equals(path)) {
				chain.setImgUrl(path);
			}
			int count = 0;
			if (chain.getId() != null) {
				count = chainMapper.updateById(chain);
			} else {
				count = chainMapper.insertSelective(chain);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区块链增加修改" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			int count = chainMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("内容说明删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<Chain> getDetail() {
		ResultBean<Chain> bean = new ResultBean<Chain>();
		try {
			List<Chain> chainList = chainMapper.selectList(new EntityWrapper<Chain>().orderBy("id", false));
			bean = new ResultBean<Chain>(chainList.get(0));
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<Chain>(e);
			logger.info("前端区块链详情查询：" + e.getMessage());
			return bean;
		}
	}

}
