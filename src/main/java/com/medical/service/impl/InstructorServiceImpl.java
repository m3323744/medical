package com.medical.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.InstructorMapper;
import com.medical.entity.Instructor;
import com.medical.entity.ResultBean;
import com.medical.service.InstructorService;

@Service
@Transactional
public class InstructorServiceImpl implements InstructorService {
	Logger logger = LoggerFactory.getLogger(InstructorServiceImpl.class);
	@Resource
	private InstructorMapper instructorMapper;

	@Override
	public ResultBean<List<Instructor>> getCombox(int serviceId) {
		ResultBean<List<Instructor>> bean = new ResultBean<List<Instructor>>();
		try {
			List<Instructor> list = instructorMapper.selectList(new EntityWrapper<Instructor>().eq("service_id", serviceId));
			bean = new ResultBean<List<Instructor>>(list);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<List<Instructor>>(e);
			logger.info("前端健康指导师下拉框查询：" + e.getMessage());
			return bean;
		}
	}
}
