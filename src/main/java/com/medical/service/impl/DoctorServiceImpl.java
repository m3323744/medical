package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.DoctorMapper;
import com.medical.dao.DoctorTypeMapper;
import com.medical.dao.DoctorWorkMapper;
import com.medical.dao.IllnessTypeMapper;
import com.medical.entity.Doctor;
import com.medical.entity.DoctorType;
import com.medical.entity.DoctorWork;
import com.medical.entity.IllnessType;
import com.medical.entity.ResultBean;
import com.medical.service.DoctorService;

@Service
@Transactional
public class DoctorServiceImpl implements DoctorService {
	Logger logger = LoggerFactory.getLogger(DoctorServiceImpl.class);
	@Resource
	private DoctorMapper dao;

	@Resource
	private DoctorTypeMapper dDao;

	@Resource
	private DoctorWorkMapper wDao;

	@Resource
	private IllnessTypeMapper iDao;

	@Override
	public Map<String, Object> getList(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		try {
			List<Doctor> data = dao.selectPage(new Page<Doctor>(pageIndex, size), new EntityWrapper<Doctor>().like("name", name).eq("status", "1"));
			// 获取总条数，并释放资源
			int total = dao.selectCount(new EntityWrapper<Doctor>().like("name", name).eq("status", "1"));
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages = (total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			map.put("name", name);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师加入列表查询：" + e.getMessage());
			return null;
		}
	}

	@Override
	public Map<String, Object> getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> map = new HashMap<>();
			Doctor doc = dao.selectById(id);
			DoctorType type = dDao.selectById(doc.getTypeId());
			List<DoctorWork> dwList = wDao.selectList(new EntityWrapper<DoctorWork>().eq("doctor_id", id));
			IllnessType ill = iDao.selectById(doc.getIllnessTypeId());
			map.put("doctor", doc);
			map.put("type", type);
			map.put("dwList", dwList);
			map.put("ill", ill);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("加入医师详情：" + e.getMessage());
			return null;
		}
	}

	@Override
	public int updateStates(int id, int states) {
		// TODO Auto-generated method stub
		try {
			int count = dao.updateStates(states, id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("加入医师审核：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int deleteDoc(int id) {
		// TODO Auto-generated method stub
		try {
			wDao.delete(new EntityWrapper<DoctorWork>().eq("doctor_id", id));
			return dao.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("加入医师删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> toAdd(Map<String, Object> map) {
		List<DoctorType> typeList = dDao.selectList(new EntityWrapper<DoctorType>());
		List<IllnessType> illnessTypeList = iDao.selectList(new EntityWrapper<IllnessType>());
		map.put("typeList", typeList);
		map.put("illnessTypeList", illnessTypeList);
		return map;

	}

	@Override
	public int add(Doctor doctor, String path) {
		// TODO Auto-generated method stub
		try {
			doctor.setHeadUrl(path);
			doctor.setSource(1);// 数据来源：1=后台录入；2=用户申请
			doctor.setStatus(3);// 状态：1=待审核；2=审核未通过；3=审核通过
			doctor.setCreateTime(new Date());
			int count = dao.insertSelective(doctor);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师后台增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;

		name = "%" + name + "%";
		List<Map<String, Object>> data = dao.getListByPage(name, (pageIndex - 1) * size, size);
		int total = dao.getListCount(name);

		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Doctor doctor = dao.selectById(id);
			List<DoctorType> typeList = dDao.selectList(new EntityWrapper<>());
			List<IllnessType> illnessTypeList = iDao.selectList(new EntityWrapper<>());
			map.put("doctor", doctor);
			map.put("typeList", typeList);
			map.put("illnessTypeList", illnessTypeList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(Doctor doctor, String path) {
		// TODO Auto-generated method stub
		try {
			if (!("").equals(path)) {
				doctor.setHeadUrl(path);
			}
			int count = dao.updateById(doctor);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<Doctor> addDoctor(String illness, String typeName, String workId, Doctor doc) {
		// TODO Auto-generated method stub
		ResultBean<Doctor> bean = null;
		try {
			Doctor doctor = dao.isExistDoc(doc.getUserId());
			DoctorType dt = dDao.selectByName(typeName);
			IllnessType ill = iDao.selectByName(illness);
			doc.setTypeId(dt.getId());
			doc.setIllnessTypeId(ill.getId());
			doc.setStatus(1);
			doc.setSource(2);
			if (doctor != null) {
				doc.setId(doctor.getId());
				doc.setCreateTime(doctor.getCreateTime());
				dao.updateById(doc);
			} else {
				doc.setCreateTime(new Date());
				dao.insert(doc);
			}
			String[] work = workId.split(",");
			for (String i : work) {
				if (!i.equals("")) {
					wDao.editDoctorId(doc.getId(), Integer.parseInt(i));
				}
			}
			bean = new ResultBean<Doctor>(doc);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean = new ResultBean<Doctor>(e);
			logger.info("小程序医师加入：" + e.getMessage());
		}
		return bean;
	}

	@Override
	public ResultBean<Map<String, Object>> isExist(int userId) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		ResultBean<Map<String, Object>> bean = null;
		Doctor doc = dao.isExistDoc(userId);
		if (doc == null) {
			return new ResultBean<Map<String, Object>>(new Exception());
		} else {
			map.put("doctor", doc);
			List<DoctorWork> list = wDao.selectList(new EntityWrapper<DoctorWork>().eq("doctor_id", doc.getId()));
			map.put("work", list);
			bean = new ResultBean<Map<String, Object>>(map);
		}
		return bean;
	}

	@Override
	public Map<String, Object> getList(int pageIndex, int illnessTypeId, String name, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			int size = 10;
			if (name != null) {
				name = "%" + name + "%";
			}
			List<Map<String, Object>> data = dao.getDoctorListByPage(name, illnessTypeId, (pageIndex - 1) * size, size);

			// 获取总条数，并释放资源
			int total = dao.getDoctorListCount(name, illnessTypeId);
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages = (total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info("前端服务团队列表查询：" + e.getMessage());
			return map;
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getDetail(int id) {
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>();
		Map<String, Object> map = new HashMap<>();
		try {
			map = dao.getDetail(id);
			bean = new ResultBean<Map<String, Object>>(map);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<Map<String, Object>>(e);
			logger.info("前端服务团队详情查询：" + e.getMessage());
			return bean;
		}
	}

	@Override
	public ResultBean<List<Doctor>> getDoctorCombox() {
		ResultBean<List<Doctor>> bean = new ResultBean<List<Doctor>>();
		try {
			List<Doctor> list = dao.selectList(new EntityWrapper<Doctor>().eq("status", 3));
			bean = new ResultBean<List<Doctor>>(list);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<List<Doctor>>(e);
			logger.info("前端服务团队下拉框查询：" + e.getMessage());
			return bean;
		}
	}
}
