package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.InsuranceDetailMapper;
import com.medical.entity.InsuranceDetail;
import com.medical.entity.ResultBean;
import com.medical.service.InsuranceDetailService;

@Service
@Transactional
public class InsuranceDetailServiceImpl implements InsuranceDetailService {
	Logger logger = LoggerFactory.getLogger(InsuranceDetailServiceImpl.class);

	@Resource
	private InsuranceDetailMapper insuranceDetailMapper;

	@Override
	public Map<String, Object> query(int type, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<InsuranceDetail> data = insuranceDetailMapper.selectPage(new Page<InsuranceDetail>(pageIndex, size), new EntityWrapper<InsuranceDetail>().eq("type", type).orderBy("id", false));
		// 获取总条数，并释放资源
		int total = insuranceDetailMapper.selectCount(new EntityWrapper<InsuranceDetail>().eq("type", type));
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public int add(InsuranceDetail insuranceDetail) {
		// TODO Auto-generated method stub
		try {
			int count = insuranceDetailMapper.insert(insuranceDetail);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险报销说明、福利说明增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> toAdd(int type, Map<String, Object> map) {
		String log = "";
		if (type == 1) {
			log = "保险报销说明详情";
		} else if (type == 2) {
			log = "福利说明详情";
		} else if (type == 3) {
			log = "使用条款详情";
		} else if (type == 4) {
			log = "隐私权政策详情";
		}
		// TODO Auto-generated method stub
		try {
			InsuranceDetail entity = new InsuranceDetail();
			entity.setType(type);

			InsuranceDetail insuranceDetail = new InsuranceDetail();
			InsuranceDetail detail = insuranceDetailMapper.selectOne(entity);
			if (detail != null) {
				insuranceDetail = detail;
			}
			map.put("insuranceDetail", insuranceDetail);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info(log + e.getMessage());
		}
		return map;
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			InsuranceDetail insuranceDetail = insuranceDetailMapper.selectById(id);
			map.put("insuranceDetail", insuranceDetail);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险报销说明、福利说明详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(InsuranceDetail insuranceDetail) {
		String log = "";
		if (insuranceDetail.getType() == 1) {
			log = "保险报销说明详情";
		} else if (insuranceDetail.getType() == 2) {
			log = "福利说明详情";
		} else if (insuranceDetail.getType() == 3) {
			log = "使用条款详情";
		} else if (insuranceDetail.getType() == 4) {
			log = "隐私权政策详情";
		}
		// TODO Auto-generated method stub
		try {
			int count = 0;
			if (insuranceDetail.getId() != null) {
				count = insuranceDetailMapper.updateById(insuranceDetail);
			} else {
				count = insuranceDetailMapper.insertSelective(insuranceDetail);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info(log + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			int count = insuranceDetailMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险报销说明、福利说明删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<InsuranceDetail> getDetail(int type) {
		ResultBean<InsuranceDetail> bean = new ResultBean<InsuranceDetail>();
		try {
			List<InsuranceDetail> doctorList = insuranceDetailMapper.selectList(new EntityWrapper<InsuranceDetail>().eq("type", type).orderBy("id", false));
			bean = new ResultBean<InsuranceDetail>(doctorList.get(0));
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<InsuranceDetail>(e);
			logger.info("前端报销说明、福利说明详情查询：" + e.getMessage());
			return bean;
		}
	}

}
