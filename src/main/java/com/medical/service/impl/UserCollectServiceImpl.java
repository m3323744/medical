package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.ArticleMapper;
import com.medical.dao.UerCollectMapper;
import com.medical.entity.Article;
import com.medical.entity.ResultBean;
import com.medical.entity.UerCollect;
import com.medical.service.UserCollectService;

@Service
@Transactional
public class UserCollectServiceImpl implements UserCollectService {
	Logger logger  =  LoggerFactory.getLogger(UserCollectServiceImpl.class);
	@Resource
	private UerCollectMapper dao;
	
	@Resource
	private ArticleMapper aDao;

	@Override
	public ResultBean<Integer> addCollect(UerCollect coll) {
		// TODO Auto-generated method stub
		ResultBean<Integer> bean=null;
		try {
			UerCollect collect=dao.checkCollect(coll);
			if(collect==null) {
				int count=dao.insert(coll);
				bean=new ResultBean<Integer>(count);
			}else {
				int count=dao.deleteById(collect.getId());
				bean=new ResultBean<Integer>(count);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<Integer>(e);
		}
		return bean;
	}

	@Override
	public ResultBean<Integer> checkCollect(UerCollect coll) {
		// TODO Auto-generated method stub
		ResultBean<Integer> bean=null;
		try {
			UerCollect collect=dao.checkCollect(coll);
			if(collect!=null) {
				bean=new ResultBean<Integer>(1);
			}else {
				bean=new ResultBean<Integer>(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<Integer>(e);
		}
		
		return bean;
	}

	@Override
	public ResultBean<Map<String, Object>> getList(int userId,int pageIndex) {
		// TODO Auto-generated method stub
		ResultBean<Map<String, Object>> bean=null;
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			List<Article> data=aDao.selectByUserId(userId, (pageIndex - 1) * 10);
			// 获取总条数，并释放资源
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			bean=new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("小程序用户收藏查询："+e.getMessage());
			bean=new ResultBean<>(e);
			
		}
		return bean;
	}
	
}
