package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.ArticleMapper;
import com.medical.dao.ArticleTypeMapper;
import com.medical.entity.Article;
import com.medical.entity.ArticleType;
import com.medical.service.ArticleTypeService;

@Service
@Transactional
public class ArticleTypeServiceImpl implements ArticleTypeService {
	Logger logger  =  LoggerFactory.getLogger(ArticleTypeServiceImpl.class);
	@Resource
	private ArticleTypeMapper dao;
	
	@Resource
	private ArticleMapper aDao;

	@Override
	public Map<String, Object> getList(int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size=10;
		try {
			List<ArticleType> data=dao.selectPage(new Page<ArticleType>(pageIndex, size), new EntityWrapper<ArticleType>());
			// 获取总条数，并释放资源
			int total = dao.selectCount(new EntityWrapper<ArticleType>());
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages=(total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("健康指南分类列表查询："+e.getMessage());
			return null;
		}
	}

	@Override
	public int delArtType(int id) {
		// TODO Auto-generated method stub
		try {
			aDao.delete(new EntityWrapper<Article>().eq("type_id", id));
			int count=dao.deleteByPrimaryKey(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("健康指南分类删除："+e.getMessage());
			return 0;
		}
	}

	@Override
	public ArticleType getDateils(int id) {
		// TODO Auto-generated method stub
		ArticleType at=dao.selectById(id);
		return at;
	}

	@Override
	public int updateArtType(ArticleType art) {
		// TODO Auto-generated method stub
		try {
			List<ArticleType> list=dao.selectList(new EntityWrapper<ArticleType>().eq("name", art.getName()));
			if(list.size()>0) {
				return 0;
			}else {
				int count=dao.updateAllColumnById(art);
				return count;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("健康指南分类更新："+e.getMessage());
			return 0;
		}
	}

	@Override
	public int addUser(ArticleType art) {
		// TODO Auto-generated method stub
		try {
			List<ArticleType> list=dao.selectList(new EntityWrapper<ArticleType>().eq("name", art.getName()));
			if(list.size()==0) {
				int count=dao.insert(art);
				return count;
			}else {
				return 0;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("健康指南分类添加："+e.getMessage());
			return 0;
		}
	}

	@Override
	public Boolean detectionType(String type) {
		// TODO Auto-generated method stub
		try {
			List<ArticleType> list=dao.selectList(new EntityWrapper<ArticleType>().eq("name", type));
			if(list.size()>0) {
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("健康指南分类名检索："+e.getMessage());
			return false;
		}
	}

	@Override
	public List<ArticleType> getAll() {
		// TODO Auto-generated method stub
		return dao.selectList(new EntityWrapper<ArticleType>());
	}
	
}
