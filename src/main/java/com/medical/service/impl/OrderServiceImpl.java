package com.medical.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.OrderMapper;
import com.medical.entity.ResultBean;
import com.medical.service.OrderService;
import com.medical.util.MapUtils;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
	Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Resource
	private OrderMapper orderMapper;

	@Override
	public Map<String, Object> query(String beginTime,String endTime, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<Map<String, Object>> data = orderMapper.getListByPage(beginTime, endTime, (pageIndex - 1) * size, size);
		// 获取总条数，并释放资源
		int total = orderMapper.getListCount(beginTime,endTime);
		
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public ResultBean<Map<String, Object>> getList(int userId, int pageIndex,Double latitude,Double longitude) {
		// TODO Auto-generated method stub
		ResultBean<Map<String, Object>> bean=null;
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			List<Map<String, Object>> data=orderMapper.getByUserId(userId, (pageIndex - 1) * 10);
			List<Map<String, Object>> list=new ArrayList<>();
			for(Map<String, Object> da:data) {
				Map<String, Object> dat=new HashMap<String, Object>();
				dat.put("id", da.get("id"));
				dat.put("order_no", da.get("order_no"));
				dat.put("amounts", da.get("amounts"));
				dat.put("create_time", da.get("create_time"));
				dat.put("img_url", da.get("img_url"));
				dat.put("name", da.get("name"));
				Double lat=Double.parseDouble(da.get("latitude").toString());
				Double lng=Double.parseDouble(da.get("longitude").toString());
				Double ju=MapUtils.GetDistance(lat, lng, latitude, longitude);
				dat.put("ju", ju);
				list.add(dat);
			}
			// 获取总条数，并释放资源
			map.put("data", list);
			map.put("pageIndex", pageIndex);
			bean=new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("小程序用户订单查询："+e.getMessage());
			bean=new ResultBean<>(e);
			
		}
		return bean;
	}


}
