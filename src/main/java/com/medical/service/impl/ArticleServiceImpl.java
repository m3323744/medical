package com.medical.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.ArticleMapper;
import com.medical.dao.ArticleTypeMapper;
import com.medical.entity.Article;
import com.medical.entity.ArticleType;
import com.medical.entity.ResultBean;
import com.medical.service.ArticleService;
import com.medical.util.UploadUtil;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
	Logger logger = LoggerFactory.getLogger(ArticleServiceImpl.class);

	@Resource
	private ArticleMapper dao;

	@Resource
	private ArticleTypeMapper aDao;

	ResultBean<List<ArticleType>> bean = null;

	@Value("${web.upload-path}")
	private String uploadPath;

	@Autowired
	private HttpServletRequest request;

	@Override
	public Map<String, Object> getList(int typeId, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		try {
			String sql = "";
			if (typeId != 0) {
				sql = " AND a.type_id=" + typeId;
			}
			int index = (pageIndex - 1) * size;
			List<Map<String, Object>> data = dao.getList(sql, index, size);
			int total = dao.getTotalCount(sql);
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages = (total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			map.put("typeId", typeId);
			map.put("typeList", aDao.selectList(new EntityWrapper<ArticleType>()));
			StringBuffer url1 = request.getRequestURL();
			String tempContextUrl = url1.delete(url1.length() - request.getRequestURI().length(), url1.length()).append("/").toString();
			map.put("tempContextUrl", tempContextUrl);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("资讯列表查询：" + e.getMessage());
			return null;
		}
	}

	@Override
	public int delArtcle(int id) {
		// TODO Auto-generated method stub
		try {
			int count = dao.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("资讯删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Article getDateils(int id) {
		// TODO Auto-generated method stub
		try {
			Article art = dao.selectById(id);
			art.setImgUrl(art.getImgUrl());
			return art;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("资讯详细查询：" + e.getMessage());
			return null;
		}
	}

	@Override
	public int updateArticle(Article art, MultipartFile file, HttpServletRequest request) {
		// TODO Auto-generated method stub
		try {
			if (!file.isEmpty() && file.getSize() > 0) {
				String url = UploadUtil.uploadYa(uploadPath, request, file);
				art.setImgUrl(url);
			}

			int count = dao.updateAllColumnById(art);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("资讯更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int addArticle(Article art, MultipartFile file, HttpServletRequest request) {
		// TODO Auto-generated method stub
		try {
			String url = UploadUtil.uploadYa(uploadPath, request, file);
			
			art.setImgUrl(url);
			art.setCreateTime(new Date());
			int count = dao.insert(art);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("资讯添加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int recommend(int id, int recommend) {
		// TODO Auto-generated method stub
		try {
			// Article art=dao.selectById(id);
			// art.setIsRecommend(true);
			// art.setCreateTime(new Date());
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = sdf.format(date);
			int count = dao.recommend(recommend, time, id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("资讯推荐：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<List<ArticleType>> getType() {
		// TODO Auto-generated method stub
		try {
			List<ArticleType> list = aDao.selectList(new EntityWrapper<ArticleType>());
			bean = new ResultBean<List<ArticleType>>(list);
			return bean;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean = new ResultBean<List<ArticleType>>(e);
			logger.info("前端资讯分类列表查询：" + e.getMessage());
			return bean;
		}
	}

	@Override
	public ResultBean<List<Article>> getByKeyWord(String keyWord) {
		// TODO Auto-generated method stub
		ResultBean<List<Article>> rBean = null;
		try {
			List<Article> list = dao.selectList(new EntityWrapper<Article>().where("title like '%" + keyWord + "%'"));
			rBean = new ResultBean<List<Article>>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rBean = new ResultBean<List<Article>>(e);
		}
		return rBean;
	}

}
