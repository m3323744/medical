package com.medical.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.IllnessTypeMapper;
import com.medical.dao.UserIllnessMapper;
import com.medical.entity.IllnessType;
import com.medical.entity.ResultBean;
import com.medical.entity.UserIllness;
import com.medical.service.UserIllnessService;

@Service
@Transactional
public class UserIllnessServiceImpl implements UserIllnessService {
	Logger logger  =  LoggerFactory.getLogger(UserIllnessServiceImpl.class);
	@Resource
	private UserIllnessMapper dao;
	
	@Resource
	private IllnessTypeMapper iDao;
	
	@Override
	public ResultBean<Map<String, Object>> getList(int userId, int pageIndex) {
		// TODO Auto-generated method stub
		ResultBean<Map<String, Object>> bean=null;
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			List<Map<String, Object>> data=dao.selectByUserId(userId, (pageIndex - 1) * 10);
			// 获取总条数，并释放资源
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			bean=new ResultBean<>(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("小程序用户过往病史查询："+e.getMessage());
			bean=new ResultBean<>(e);
			
		}
		return bean;
	}

	@Override
	public ResultBean<Integer> addIllness(UserIllness illness, String type) {
		// TODO Auto-generated method stub
		ResultBean<Integer> bean=null;
		try {
			IllnessType iness=iDao.selectByName(type);
			illness.setTypeId(iness.getId());
			int count=dao.insert(illness);
			bean=new ResultBean<Integer>(count);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<Integer>(e);
			logger.info("小程序过往病史添加："+e.getMessage());
		}
		return bean;
	}

}
