package com.medical.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.medical.dao.DoctorMapper;
import com.medical.dao.IllnessTypeMapper;
import com.medical.dao.RecipeMapper;
import com.medical.entity.Doctor;
import com.medical.entity.IllnessType;
import com.medical.entity.Recipe;
import com.medical.entity.ResultBean;
import com.medical.service.RecipeService;

@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {
	Logger logger = LoggerFactory.getLogger(RecipeServiceImpl.class);

	@Resource
	private RecipeMapper recipeMapper;
	@Resource
	private DoctorMapper doctorMapper;
	@Resource
	private IllnessTypeMapper illnessTypeMapper;

	@Override
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		name = "%" + name + "%";
		List<Map<String, Object>> data = recipeMapper.getListByPage(name, (pageIndex - 1) * size, size);

		// 获取总条数，并释放资源
		int total = recipeMapper.getListCount(name);
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> toAdd(Map<String, Object> map) {
		List<Doctor> doctorList = doctorMapper.selectList(new EntityWrapper<Doctor>());
		map.put("doctorList", doctorList);

		List<IllnessType> illnessTypeList = illnessTypeMapper.selectList(new EntityWrapper<IllnessType>());
		map.put("illnessTypeList", illnessTypeList);
		return map;

	}

	@Override
	public int add(Recipe recipe, String path) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<Recipe> list = recipeMapper.selectList(new EntityWrapper<Recipe>().eq("name", recipe.getName()));
			if (list != null && list.size() > 0) {
				count = -1;
			}
			if (list == null || list.size() == 0) {
				recipe.setImgUrl(path);
				recipe.setCreateTime(new Date());
				recipe.setStatus(1);
				count = recipeMapper.insertSelective(recipe);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Recipe recipe = recipeMapper.selectById(id);
			Doctor doctor = doctorMapper.selectById(recipe.getDoctorId());
			List<Doctor> doctorList = doctorMapper.selectList(new EntityWrapper<Doctor>());
			List<IllnessType> illnessTypeList = illnessTypeMapper.selectList(new EntityWrapper<IllnessType>());
			map.put("recipe", recipe);
			map.put("doctor", doctor);
			map.put("doctorList", doctorList);
			map.put("illnessTypeList", illnessTypeList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(Recipe recipe, String path) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<Recipe> list = recipeMapper.selectList(new EntityWrapper<Recipe>().eq("name", recipe.getName()));
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() != recipe.getId()) {
					count--;
				}
			}
			if (count >= 0) {
				if (!("").equals(path)) {
					recipe.setImgUrl(path);
				}
				count = recipeMapper.updateById(recipe);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			Recipe recipe = new Recipe();
			recipe.setId(id);
			recipe.setStatus(-1);
			int count = recipeMapper.updateById(recipe);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> getList(int pageIndex, Integer illnessTypeId, String name, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			int size = 10;
			if(name != null) {
				name = "%" + name + "%";
			}
			List<Map<String, Object>> data = recipeMapper.getListPage(illnessTypeId, name, (pageIndex - 1) * size, size);

			// 获取总条数，并释放资源
			int total = recipeMapper.getListPageCount(illnessTypeId, name);
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages = (total - 1) / size + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info("前端处方列表查询：" + e.getMessage());
			return map;
		}
	}

	@Override
	public ResultBean<Map<String, Object>> getDetail(int id) {
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>();
		try {
			Map<String, Object> map = new HashMap<>();
			Recipe recipe = recipeMapper.selectById(id);
			Doctor doctor = doctorMapper.selectById(recipe.getDoctorId());
			map.put("recipe", recipe);
			map.put("doctorName", doctor.getName());
			bean = new ResultBean<Map<String, Object>>(map);
			return bean;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			bean = new ResultBean<Map<String, Object>>(e);
			logger.info("前端处方详情查询：" + e.getMessage());
			return bean;
		}
	}
}
