package com.medical.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.AdminMapper;
import com.medical.entity.Admin;
import com.medical.service.AdminService;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {
	@Resource
	private AdminMapper dao;
	
	@Override
	public Admin getByUser(String username, String pwd) {
		// TODO Auto-generated method stub
		return dao.getByUser(username, pwd);
	}

	@Override
	public int editPwd(Admin ad) {
		// TODO Auto-generated method stub
		return dao.updateAllColumnById(ad);
	}

}
