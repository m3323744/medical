package com.medical.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.DoctorMapper;
import com.medical.dao.DoctorTypeMapper;
import com.medical.entity.Doctor;
import com.medical.entity.DoctorType;
import com.medical.entity.ResultBean;
import com.medical.service.DoctorTypeService;

@Service
@Transactional
public class DoctorTypeServiceImpl implements DoctorTypeService {
	Logger logger = LoggerFactory.getLogger(DoctorTypeServiceImpl.class);

	@Resource
	private DoctorTypeMapper doctorTypeMapper;
	@Resource
	private DoctorMapper doctorMapper;

	@Override
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int size = 10;
		List<DoctorType> data = doctorTypeMapper.selectPage(new Page<DoctorType>(pageIndex, size), new EntityWrapper<DoctorType>().like("name", name).orderBy("id", false));
		// 获取总条数，并释放资源
		int total = doctorTypeMapper.selectCount(new EntityWrapper<DoctorType>().like("name", name));
		map.put("data", data);
		map.put("pageIndex", pageIndex);
		int totalPages = (total - 1) / size + 1;
		map.put("totalPages", totalPages);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> detail(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			DoctorType doctorType = doctorTypeMapper.selectById(id);
			map.put("doctorType", doctorType);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师职业分类详情：" + e.getMessage());
		}
		return map;
	}

	@Override
	public int update(DoctorType doctorType) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<DoctorType> list = doctorTypeMapper.selectList(new EntityWrapper<DoctorType>().eq("name", doctorType.getName()));
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() != doctorType.getId()) {
					count--;
				}
			}
			if (count >= 0) {
				count = doctorTypeMapper.updateById(doctorType);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师职业分类更新：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			doctorMapper.delete(new EntityWrapper<Doctor>().eq("type_id", id));
			int count = doctorTypeMapper.deleteById(id);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师职业分类删除：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public int add(DoctorType doctorType) {
		// TODO Auto-generated method stub
		try {
			int count = 0;
			List<DoctorType> list = doctorTypeMapper.selectList(new EntityWrapper<DoctorType>().eq("name", doctorType.getName()));
			if (list != null && list.size() > 0) {
				count = -1;
			}
			if (list == null || list.size() == 0) {
				count = doctorTypeMapper.insertSelective(doctorType);
			}
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师职业分类增加：" + e.getMessage());
			return 0;
		}
	}

	@Override
	public ResultBean<List<DoctorType>> getAll() {
		// TODO Auto-generated method stub
		ResultBean<List<DoctorType>> bean=null;
		try {
			List<DoctorType> list=doctorTypeMapper.selectList(new EntityWrapper<DoctorType>());
			bean=new ResultBean<>(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("小程序医生类别查询："+e.getMessage());
			bean=new ResultBean<>(e);
		}
		return bean;
	}

}
