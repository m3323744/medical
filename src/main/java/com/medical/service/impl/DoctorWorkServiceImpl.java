package com.medical.service.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medical.dao.DoctorTypeMapper;
import com.medical.dao.DoctorWorkMapper;
import com.medical.entity.DoctorType;
import com.medical.entity.DoctorWork;
import com.medical.entity.ResultBean;
import com.medical.service.DoctorWorkService;

@Service
@Transactional
public class DoctorWorkServiceImpl implements DoctorWorkService {
	@Resource
	private DoctorWorkMapper dao;
	
	@Resource
	private DoctorTypeMapper dDao;
	
	Logger logger=LoggerFactory.getLogger(DoctorWorkServiceImpl.class);

	@Override
	public ResultBean<DoctorWork> addWork(DoctorWork dw,String type) {
		// TODO Auto-generated method stub
		ResultBean<DoctorWork> bean=null;
		try {
			DoctorType dt=dDao.selectByName(type);
			dw.setTypeId(dt.getId());
			dao.insert(dw);
			bean=new ResultBean<DoctorWork>(dw);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bean=new ResultBean<DoctorWork>(e);
			logger.info("小程序添加工作经历："+e.getMessage());
		}
		return bean;
	}
	
	
}
