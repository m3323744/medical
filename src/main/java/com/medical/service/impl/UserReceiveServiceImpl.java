package com.medical.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.medical.dao.UserReceiveMapper;
import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.entity.UserReceive;
import com.medical.service.UserReceiveService;

@Service
@Transactional
public class UserReceiveServiceImpl implements UserReceiveService {
	Logger logger = LoggerFactory.getLogger(UserReceiveServiceImpl.class);

	@Resource
	private UserReceiveMapper userReceiveMapper;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResultBean add(int userId) {
		// TODO Auto-generated method stub
		ResultBean bean = new ResultBean();
		try {
			List<UserReceive> list = userReceiveMapper.selectList(new EntityWrapper<UserReceive>().eq("user_id", userId));
			if(list == null || list.size() == 0) {
				UserReceive userReceive = new UserReceive();
				userReceive.setUserId(userId);
				userReceive.setCreateTime(new Date());
				int count = userReceiveMapper.insert(userReceive);
				if(count > 0) {
					bean.setCode(0);
					bean.setMsg("领取成功");
				}else {
					bean.setCode(1);
					bean.setMsg("领取失败");

				}
			}else {
				bean.setCode(1);
				bean.setMsg("不能重复领取");
			}
			return bean;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("前端福利领取增加：" + e.getMessage());
			bean = new ResultBean(e);
			return bean;
		}
	}

	@Override
	public Map<String, Object> getList(String name, int pageIndex, Map<String, Object> map) {
		// TODO Auto-generated method stub
		int index=(pageIndex - 1) * 10;
		map.put("name", name);
		try {
			name="%"+name+"%";
			List<Map<String, Object>> data=userReceiveMapper.getList(name, index);
			// 获取总条数，并释放资源
			int total = userReceiveMapper.getListCount(name);
			map.put("data", data);
			map.put("pageIndex", pageIndex);
			int totalPages=(total - 1) / 10 + 1;
			map.put("totalPages", totalPages);
			map.put("total", total);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("会员领取保险列表查询："+e.getMessage());
			return null;
		}
	}

}
