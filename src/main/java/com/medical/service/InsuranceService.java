package com.medical.service;

import java.util.Map;

import com.medical.entity.Insurance;
import com.medical.entity.ResultBean;

public interface InsuranceService {
	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map);

	public int add(Insurance insurance, String titleUrl, String path);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(Insurance insurance, String titleUrl, String path);

	public int delete(int id);

	public Map<String, Object> getList(int pageIndex, Map<String, Object> map);

	public ResultBean<Insurance> getDetail(int id);

}
