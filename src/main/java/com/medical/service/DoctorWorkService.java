package com.medical.service;

import com.medical.entity.DoctorWork;
import com.medical.entity.ResultBean;

public interface DoctorWorkService {
	public ResultBean<DoctorWork> addWork(DoctorWork dw,String type);
}
