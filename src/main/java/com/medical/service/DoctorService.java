package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.Doctor;
import com.medical.entity.ResultBean;

public interface DoctorService {
	public Map<String, Object> getList(String name, int pageIndex, Map<String, Object> map);

	public Map<String, Object> getDateils(int id);

	public int updateStates(int id, int states);

	public int deleteDoc(int id);

	public int add(Doctor doctor, String path);

	public Map<String, Object> toAdd(Map<String, Object> map);

	public Map<String, Object> query(String name, int pageIndex, Map<String, Object> map);

	public Map<String, Object> detail(int id, Map<String, Object> map);

	public int update(Doctor doctor, String path);

	public ResultBean<Doctor> addDoctor(String illness, String typeName, String workId, Doctor doc);

	public ResultBean<Map<String, Object>> isExist(int userId);

	public Map<String, Object> getList(int pageIndex, int illnessTypeId, String name, Map<String, Object> map);

	public ResultBean<Map<String, Object>> getDetail(int id);

	public ResultBean<List<Doctor>> getDoctorCombox();

}
