package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;

public interface OrderService {
	public Map<String, Object> query(String beginTime, String endTime, int pageIndex, Map<String, Object> map);
	
	public ResultBean<Map<String, Object>> getList(int userId,int pageIndex,Double latitude,Double longitude);
}
