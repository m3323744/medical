package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.UserIllness;

public interface UserIllnessService {
	public ResultBean<Map<String, Object>> getList(int userId,int pageIndex);
	public ResultBean<Integer> addIllness(UserIllness illness,String type);
}
