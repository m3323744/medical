package com.medical.service;

import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.UserReceive;

public interface UserReceiveService {
	public ResultBean<UserReceive> add(int userId);
	public Map<String, Object> getList(String name, int pageIndex, Map<String, Object> map);
}
