package com.medical.service;

import java.util.List;
import java.util.Map;

import com.medical.entity.ResultBean;
import com.medical.entity.User;

public interface UserService {
	public List<User> getAll();
	public Map<String, Object>  getList(String keyWord, int pageIndex,Map<String, Object> map);
	public int delUser(int id);
	public User getDateils(int id);
	public int updateUser(User use);
	public int addUser(User use);
	public Boolean detectionUsername(String username);
	public User addwxUser(User use);
	public ResultBean<User> getApiDateils(int id);
	public ResultBean<Integer> updateApiDateils(User use);
	public ResultBean<Integer> updateName(String name,String headurl,int id);
	public ResultBean<Integer> updatePhone(String phone,int id);
}
