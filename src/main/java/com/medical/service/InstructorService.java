package com.medical.service;

import java.util.List;

import com.medical.entity.Instructor;
import com.medical.entity.ResultBean;

public interface InstructorService {

	public ResultBean<List<Instructor>> getCombox(int serviceId);

}
