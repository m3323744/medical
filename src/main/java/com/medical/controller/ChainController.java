package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Chain;
import com.medical.service.ChainService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/chain")
public class ChainController {
	Logger logger = LoggerFactory.getLogger(ChainController.class);

	@Autowired
	private ChainService chainService;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = chainService.query(pageIndex, map);
			model.addAttribute("dataMap", map);
			return "chainList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("区块链列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = chainService.toAdd(map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "");
			return "chainAdd";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @ModelAttribute Chain chain, BindingResult bindingResult, Model model) {
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = chainService.add(chain, path);
		if (count > 0) {
			return "redirect:/chain/query";
		} else {
			model.addAttribute("msg", "新增失败");
			return "chainAdd";
		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = chainService.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		return "chainEdit";
	}

	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @ModelAttribute Chain chain, BindingResult bindingResult, Model model) {
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = chainService.update(chain, path);
		if (count > 0) {
			model.addAttribute("msg", "保存成功");
		} else {
			model.addAttribute("msg", "保存失败");
		}
		Map<String, Object> map = new HashMap<>();
		map = chainService.toAdd(map);
		model.addAttribute("dataMap", map);
		return "chainAdd";
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = chainService.delete(id);
		return count + "";
	}

}
