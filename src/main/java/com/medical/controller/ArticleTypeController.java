package com.medical.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medical.entity.ArticleType;
import com.medical.service.ArticleTypeService;

@Controller
@RequestMapping("/artType")
public class ArticleTypeController {
	@Autowired
	private ArticleTypeService biz;
	
	@RequestMapping("/getList")
	public String  getList(@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,Model model){
		Map<String, Object> map=new HashMap<>();
		map=biz.getList(pageIndex, map);
		model.addAttribute("artMap", map);
		return "artTypeList";
	}
	
	@RequestMapping("/delArtc")
	@ResponseBody
	public String delArtc(@RequestParam("id")int id) {
		return biz.delArtType(id)+"";
	}
	
	@RequestMapping("/artcDateils")
	@ResponseBody
	public ArticleType artcDateils(@RequestParam("id")int id){
		ArticleType art=biz.getDateils(id);
		return art;
	}
	
	@RequestMapping("/updateArtc")
	@ResponseBody
	public String updateArtc(@ModelAttribute ArticleType art) {
		int count =biz.updateArtType(art);
		return count+"";
	}
	
	@RequestMapping("/detectionName")
	@ResponseBody
	public Boolean detectionName(@RequestParam("name")String name) {
		return biz.detectionType(name);
	}
	
	@RequestMapping("/addArtcType")
	@ResponseBody
	public String addArtcType(@RequestParam("name")String name) {
		ArticleType at=new ArticleType();
		at.setName(name);
		int count=biz.addUser(at);
		return count+"";
	}
	
	@RequestMapping("/addArticle")
	public String addArticle(Model model) {
		List<ArticleType> list=biz.getAll();
		model.addAttribute("typeList", list);
		return "articleAdd";
	}
}
