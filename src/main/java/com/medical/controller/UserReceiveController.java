package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.medical.service.UserReceiveService;

@Controller
@RequestMapping("/userreceive")
public class UserReceiveController {
	@Autowired
	private UserReceiveService biz;
	
	@RequestMapping("/getList")
	public String getList(@RequestParam(value="name",defaultValue="")String name,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,Model model) {
		Map<String, Object> map=new HashMap<>();
		map=biz.getList(name, pageIndex, map);
		model.addAttribute("receiveMap", map);
		return "userReceiveList";
	}
}
