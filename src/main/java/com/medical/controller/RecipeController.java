package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Recipe;
import com.medical.service.RecipeService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/recipe")
public class RecipeController {
	Logger logger = LoggerFactory.getLogger(RecipeController.class);

	@Autowired
	private RecipeService recipeService;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = recipeService.query(name, pageIndex, map);
			map.put("name", name);
			model.addAttribute("dataMap", map);
			return "recipeList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = recipeService.toAdd(map);
			model.addAttribute("dataMap", map);
			return "recipeAdd";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file", required = true) MultipartFile file, @ModelAttribute Recipe recipe, BindingResult bindingResult, Model model) {
		String path = UploadUtil.uploadYa(uploadPath, request, file);
		int count = recipeService.add(recipe, path);
		if (count > 0) {
			return "redirect:/recipe/query";
		} else if (count < 0) {
			model.addAttribute("msg", "处方名称已存在");
			Map<String, Object> map = new HashMap<>();
			map = recipeService.toAdd(map);
			model.addAttribute("dataMap", map);
			return "recipeAdd";
		} else {
			Map<String, Object> map = new HashMap<>();
			map = recipeService.toAdd(map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "新增失败");
			return "recipeAdd";
		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = recipeService.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		return "recipeEdit";
	}

	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @RequestParam("id") int id, @ModelAttribute Recipe recipe, BindingResult bindingResult, Model model) {
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = recipeService.update(recipe, path);
		if (count > 0) {
			return "redirect:/recipe/query";
		} else if (count < 0) {
			Map<String, Object> map = new HashMap<>();
			map = recipeService.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "处方名称已存在");
			return "recipeEdit";
		} else {
			Map<String, Object> map = new HashMap<>();
			map = recipeService.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "修改失败");
			return "recipeEdit";
		}
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = recipeService.delete(id);
		return count + "";
	}

}
