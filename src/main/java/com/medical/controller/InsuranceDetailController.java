package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medical.entity.InsuranceDetail;
import com.medical.service.InsuranceDetailService;

@Controller
@RequestMapping("/insuranceDetail")
public class InsuranceDetailController {
	Logger logger = LoggerFactory.getLogger(InsuranceDetailController.class);

	@Autowired
	private InsuranceDetailService insuranceDetailService;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "type", required = true) int type, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = insuranceDetailService.query(type, pageIndex, map);
			model.addAttribute("dataMap", map);
			if (type == 1) {
				return "insuranceDetailList";
			} else {
				return "insuranceWelfareList";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险报销说明、福利说明列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model, @RequestParam(value = "type", required = true) int type) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = insuranceDetailService.toAdd(type, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "");
			String page = "";
			if (type == 1) {
				page = "insuranceDetailAdd";
			} else if (type == 2) {
				page = "insuranceWelfareAdd";
			} else if (type == 3) {
				page = "termsAdd";
			} else {
				page = "privacyAdd";
			}
			return page;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(@ModelAttribute InsuranceDetail insuranceDetail, BindingResult bindingResult, Model model) {
		int count = insuranceDetailService.add(insuranceDetail);
		if (count > 0) {
			if (insuranceDetail.getType() == 1) {
				return "redirect:/insuranceDetail/query?type=1";
			} else {
				return "redirect:/insuranceDetail/query?type=2";
			}
		} else {
			model.addAttribute("msg", "新增失败");
			if (insuranceDetail.getType() == 1) {
				return "insuranceDetailAdd";
			} else {
				return "insuranceWelfareAdd";
			}

		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, @RequestParam("type") int type, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = insuranceDetailService.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		if (type == 1) {
			return "insuranceDetailEdit";
		} else {
			return "insuranceWelfareEdit";
		}
	}

	@RequestMapping("/update")
	public String update(@RequestParam(value = "type", required = true) int type, @ModelAttribute InsuranceDetail insuranceDetail, BindingResult bindingResult, Model model) {
		int count = insuranceDetailService.update(insuranceDetail);
		if (count > 0) {
			model.addAttribute("msg", "保存成功");
		} else {
			model.addAttribute("msg", "保存失败");
		}
		Map<String, Object> map = new HashMap<>();
		map = insuranceDetailService.toAdd(type, map);
		model.addAttribute("dataMap", map);

		String page = "";
		if (type == 1) {
			page = "insuranceDetailAdd";
		} else if (type == 2) {
			page = "insuranceWelfareAdd";
		} else if (type == 3) {
			page = "termsAdd";
		} else {
			page = "privacyAdd";
		}

		return page;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = insuranceDetailService.delete(id);
		return count + "";
	}

}
