package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medical.entity.Area;
import com.medical.service.AreaService;

@Controller
@RequestMapping("/area")
public class AreaController {
	Logger logger = LoggerFactory.getLogger(AreaController.class);

	@Autowired
	private AreaService areaService;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = areaService.query(name, pageIndex, map);
			map.put("name", name);
			model.addAttribute("dataMap", map);
			return "areaList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("处方分类列表查询：" + e.getMessage());
			return "";
		}
	}


	@RequestMapping("/update")
	@ResponseBody
	public String update(@ModelAttribute Area area, Model model) {
		int count = areaService.update(area);
		return count+"";
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = areaService.delete(id);
		return count + "";
	}

	@RequestMapping("/add")
	@ResponseBody
	public String add(@ModelAttribute Area area, Model model) {
		int count = areaService.add(area);
		return count+"";
	}

}
