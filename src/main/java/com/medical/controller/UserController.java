package com.medical.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService biz;
	
	@Autowired
    private RedisTemplate redisTemplate;
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/getAll")
	@ResponseBody
	public List<User> getAll(){
		
		List<User> list= biz.getAll();
		redisTemplate.opsForValue().set("num", "123");
		redisTemplate.expire("num", 123456, TimeUnit.SECONDS);
		return list;
	}
	
	@RequestMapping("/test")
	@ResponseBody
	public String test(){
		
		return redisTemplate.opsForValue().get("num").toString();
	}
	
	@RequestMapping("/getList")
	public String  getList(@RequestParam(value="keyWord",defaultValue="")String keyWord,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex,Model model){
		Map<String, Object> map=new HashMap<>();
		map=biz.getList(keyWord, pageIndex, map);
		model.addAttribute("userMap", map);
		return "userList";
	}
	
	@RequestMapping("/delUser")
	@ResponseBody
	public String delUser(@RequestParam("id")int id) {
		return biz.delUser(id)+"";
	}
	
	@RequestMapping("/userDateils")
	public String userDateils(@RequestParam("id")int id,Model model){
		User user=biz.getDateils(id);
		model.addAttribute("user", user);
		return "userEdit";
	}
	
	@RequestMapping("/updateUser")
	public String updateUser(@ModelAttribute User use,Model model) {
		int count =biz.updateUser(use);
		if(count>0) {
			return "redirect:/user/getList";
		}else {
			model.addAttribute("msg", "修改失败！");
			return "userEdit";
		}
	}
	
	@RequestMapping("/detectionPhone")
	@ResponseBody
	public Boolean detectionPhone(@RequestParam("name")String name) {
		return biz.detectionUsername(name);
	}
	
	
}
