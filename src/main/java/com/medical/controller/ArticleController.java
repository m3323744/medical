package com.medical.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Article;
import com.medical.entity.ArticleType;
import com.medical.service.ArticleService;
import com.medical.service.ArticleTypeService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/article")
public class ArticleController {
	@Autowired
	private ArticleService biz;

	@Autowired
	private ArticleTypeService aBiz;

	@Value("${web.upload-path}")
	private String uploadPath;
	
	@Autowired
	private HttpServletRequest request;

	@RequestMapping("/getList")
	public String getList(@RequestParam(value = "typeId", defaultValue = "0") int typeId, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = biz.getList(typeId, pageIndex, map);
		model.addAttribute("artMap", map);
		return "articleList";
	}

	@RequestMapping("/delArtc")
	@ResponseBody
	public String delArtc(@RequestParam("id") int id) {
		return biz.delArtcle(id) + "";
	}

	@RequestMapping("/artcDateils")
	public String artcDateils(@RequestParam("id") int id, Model model) {
		Article art = biz.getDateils(id);
		model.addAttribute("article", art);
		List<ArticleType> list = aBiz.getAll();
		model.addAttribute("typeList", list);
		StringBuffer url1 = request.getRequestURL();
		String tempContextUrl = url1.delete(url1.length() - request.getRequestURI().length(), url1.length()).append("/").toString();
		model.addAttribute("tempContextUrl", tempContextUrl);
		return "articleEdit";
	}

	@RequestMapping("/updateArtc")
	public String updateArtc(@ModelAttribute Article art, Model model, @RequestParam("file") MultipartFile file, HttpServletRequest request) {
		int count = biz.updateArticle(art, file, request);
		if (count > 0) {
			return "redirect:/article/getList";
		} else {
			List<ArticleType> list = aBiz.getAll();
			model.addAttribute("typeList", list);
			model.addAttribute("msg", "修改失败！");
			return "articleEdit";
		}
	}

	@RequestMapping("/addArtc")
	public String addArtc(@ModelAttribute Article art, Model model, @RequestParam("file") MultipartFile file, HttpServletRequest request) {
		int count = biz.addArticle(art, file, request);
		if (count > 0) {
			return "redirect:/article/getList";
		} else {
			List<ArticleType> list = aBiz.getAll();
			model.addAttribute("typeList", list);
			model.addAttribute("msg", "添加失败！");
			return "articleAdd";
		}
	}

	@RequestMapping("/updateStatus")
	@ResponseBody
	public String updateStatus(int status, int id) {
		int count = biz.recommend(id, status);
		return count + "";
	}

	@RequestMapping("/upload")
	@ResponseBody
	public Map<String, Object> upload(MultipartFile file, HttpServletRequest request) {
		String str = UploadUtil.uploadYa(uploadPath, request, file);
		StringBuffer url = request.getRequestURL();
		String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
		Map<String, Object> map = new HashMap<>();
		map.put("code", 0);
		map.put("msg", "成功");
		Map<String, Object> map1 = new HashMap<>();
		map1.put("src", tempContextUrl + str);
		map.put("data", map1);
		return map;
	}
}
