package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.ServiceCenter;
import com.medical.service.ServiceCenterService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/serviceCenter")
public class ServiceCenterController {
	Logger logger = LoggerFactory.getLogger(ServiceCenterController.class);

	@Autowired
	private ServiceCenterService serviceCenterService;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "areaId", defaultValue = "0") int areaId, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = serviceCenterService.query(name, areaId, pageIndex, map);
			map.put("name", name);
			map.put("areaId", areaId);
			model.addAttribute("dataMap", map);
			return "serviceCenterList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("服务中心列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = serviceCenterService.toAdd(map);
			model.addAttribute("dataMap", map);
			return "serviceCenterAdd";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "instructor", defaultValue = "") String[] instructors, @RequestParam(value = "level", defaultValue = "") String[] levels, @ModelAttribute ServiceCenter serviceCenter, BindingResult bindingResult, Model model) {
		String path = UploadUtil.uploadYa(uploadPath, request, file);
		int count = serviceCenterService.add(serviceCenter, path, instructors, levels);
		if (count > 0) {
			return "redirect:/serviceCenter/query";
		} else if (count < 0) {
			model.addAttribute("msg", "服务中心名称已存在");
			return "serviceCenterAdd";
		} else {
			model.addAttribute("msg", "新增失败");
			return "serviceCenterAdd";
		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = serviceCenterService.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		return "serviceCenterEdit";
	}

	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @RequestParam("id") int id, @RequestParam(value = "instructors", defaultValue = "") String[] instructors, @RequestParam(value = "levels", defaultValue = "") String[] levels, @ModelAttribute ServiceCenter serviceCenter, BindingResult bindingResult, Model model) {
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = serviceCenterService.update(serviceCenter, path, instructors, levels);
		if (count > 0) {
			return "redirect:/serviceCenter/query";
		} else if (count < 0) {
			Map<String, Object> map = new HashMap<>();
			map = serviceCenterService.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "服务中心名称已存在");
			return "serviceCenterEdit";
		} else {
			Map<String, Object> map = new HashMap<>();
			map = serviceCenterService.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "修改失败");
			return "serviceCenterEdit";
		}
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = serviceCenterService.delete(id);
		return count + "";
	}

	@RequestMapping("/updateStatus")
	@ResponseBody
	public String updateStatus(@RequestParam("id") int id, @RequestParam("isTop") int isTop, Model model) {
		int count = serviceCenterService.updateStatus(id, isTop);
		return count + "";
	}

}
