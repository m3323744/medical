package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medical.entity.DoctorType;
import com.medical.service.DoctorTypeService;

@Controller
@RequestMapping("/doctorType")
public class DoctorTypeController {
	Logger logger = LoggerFactory.getLogger(DoctorTypeController.class);

	@Autowired
	private DoctorTypeService doctorTypeService;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = doctorTypeService.query(name, pageIndex, map);
			map.put("name", name);
			model.addAttribute("dataMap", map);
			return "doctorTypeList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师职业分类列表查询：" + e.getMessage());
			return "";
		}
	}


	@RequestMapping("/update")
	@ResponseBody
	public String update(@ModelAttribute DoctorType doctorType, Model model) {
		int count = doctorTypeService.update(doctorType);
		return count+"";
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = doctorTypeService.delete(id);
		return count + "";
	}

	@RequestMapping("/add")
	@ResponseBody
	public String add(@ModelAttribute DoctorType doctorType, Model model) {
		int count = doctorTypeService.add(doctorType);
		return count+"";
	}

}
