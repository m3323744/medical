package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Img;
import com.medical.service.ImgService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/img")
public class ImgController {
	Logger logger = LoggerFactory.getLogger(ImgController.class);

	@Autowired
	private ImgService imgService;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = imgService.query(pageIndex, map);
			model.addAttribute("dataMap", map);
			return "imgList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("图片列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		try {
			return "imgAdd";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file", required = true) MultipartFile file, @ModelAttribute Img img, BindingResult bindingResult, Model model) {
		String path = UploadUtil.uploadYa(uploadPath, request, file);
		int count = imgService.add(img, path);
		if (count > 0) {
			return "redirect:/img/query";
		} else {
			model.addAttribute("msg", "新增失败");
			return "imgAdd";
		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = imgService.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		return "imgEdit";
	}

	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @RequestParam("id") int id, @ModelAttribute Img img, BindingResult bindingResult, Model model) {
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = imgService.update(img, path);
		if (count > 0) {
			return "redirect:/img/query";
		} else {
			Map<String, Object> map = new HashMap<>();
			map = imgService.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "修改失败");
			return "imgEdit";
		}
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = imgService.delete(id);
		return count + "";
	}

	@RequestMapping("/updateStatus")
	@ResponseBody
	public String updateStatus(int id, int isShow) {
		int count = imgService.updateStatus(id, isShow);
		return count + "";
	}
}
