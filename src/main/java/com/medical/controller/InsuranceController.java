package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Insurance;
import com.medical.service.InsuranceService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/insurance")
public class InsuranceController {
	Logger logger = LoggerFactory.getLogger(InsuranceController.class);

	@Autowired
	private InsuranceService insuranceService;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = insuranceService.query(name, pageIndex, map);
			model.addAttribute("dataMap", map);
			return "insuranceList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("保险列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		try {
			return "insuranceAdd";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "titleFile", required = true) MultipartFile titleFile, @ModelAttribute Insurance insurance, BindingResult bindingResult, Model model) {
		String titleUrl = UploadUtil.uploadYa(uploadPath, request, titleFile);
		String path = UploadUtil.uploadYa(uploadPath, request, file);
		int count = insuranceService.add(insurance, titleUrl, path);
		if (count > 0) {
			return "redirect:/insurance/query";
		} else if (count < 0) {
			model.addAttribute("msg", "保险名称已存在");
			return "insuranceAdd";
		} else {
			model.addAttribute("msg", "新增失败");
			return "insuranceAdd";
		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = insuranceService.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		return "insuranceEdit";
	}

	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @RequestParam(value = "titleFile", required = true) MultipartFile titleFile, @RequestParam("id") int id, @ModelAttribute Insurance insurance, BindingResult bindingResult, Model model) {
		String titleUrl = "";
		if (!titleFile.isEmpty()) {
			titleUrl = UploadUtil.uploadYa(uploadPath, request, titleFile);
		}
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = insuranceService.update(insurance, titleUrl, path);
		if (count > 0) {
			return "redirect:/insurance/query";
		} else {
			Map<String, Object> map = new HashMap<>();
			map = insuranceService.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "修改失败");
			return "insuranceEdit";
		}
	}

	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam("id") int id) {
		int count = insuranceService.delete(id);
		return count + "";
	}

}
