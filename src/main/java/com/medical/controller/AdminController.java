package com.medical.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medical.entity.Admin;
import com.medical.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private AdminService biz;
	
	@RequestMapping("/login")
	public String login(String username,String password,Model model,HttpServletRequest request) {
		Admin ad=biz.getByUser(username, password);
		if(ad!=null) {
			request.getSession().setAttribute("emp", ad);
			return "index";
		}else {
			model.addAttribute("msg", "帐号密码错误");
			return "login";
		}
	}
	
	@RequestMapping("/editPwd")
	@ResponseBody
	public String editPwd(String pwd,HttpServletRequest request) {
		Admin ad=(Admin)request.getSession().getAttribute("emp");
		ad.setPassword(pwd);
		int count=biz.editPwd(ad);
		return count+"";
		
	}
}
