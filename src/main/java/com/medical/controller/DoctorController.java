package com.medical.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.Doctor;
import com.medical.service.DoctorService;
import com.medical.util.UploadUtil;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

	private static final Logger logger = LoggerFactory.getLogger(DoctorController.class);

	@Autowired
	private DoctorService biz;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/getList")
	public String getList(@RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = biz.getList(name, pageIndex, map);
		model.addAttribute("doctorMap", map);
		return "doctorList";
	}

	@RequestMapping("/delDoc")
	@ResponseBody
	public String delDoc(@RequestParam("id") int id) {
		return biz.deleteDoc(id) + "";
	}

	@RequestMapping("/getDateils")
	public String getDateils(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = biz.getDateils(id);
		model.addAttribute("doctor", map);
		return "doctorDateil";
	}

	@RequestMapping("/updateStates")
	public String updateStates(@RequestParam("id") int id, @RequestParam("states") int states, Model model) {
		int count = biz.updateStates(id, states);
		if (count > 0) {
			return "redirect:/doctor/getList";
		} else {
			model.addAttribute("msg", "修改失败");
			return "redirect:/doctor/getDateils?id=" + id;
		}

	}

	@RequestMapping("/toAdd")
	public String toAdd(Model model) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = biz.toAdd(map);
			model.addAttribute("dataMap", map);
			return "doctorAdd";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file", required = true) MultipartFile file, @ModelAttribute Doctor doctor, BindingResult bindingResult, Model model) {
		String path = UploadUtil.uploadYa(uploadPath, request, file);
		int count = biz.add(doctor, path);
		if (count > 0) {
			return "redirect:/doctor/getList";
		} else {
			model.addAttribute("msg", "新增失败");
			return "doctorAdd";
		}
	}

	@RequestMapping("/query")
	public String query(Model model, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		try {
			Map<String, Object> map = new HashMap<>();
			map = biz.query(name, pageIndex, map);
			map.put("name", name);
			model.addAttribute("dataMap", map);
			return "drList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("医师列表查询：" + e.getMessage());
			return "";
		}
	}

	@RequestMapping("/detail")
	public String detail(@RequestParam("id") int id, Model model) {
		Map<String, Object> map = new HashMap<>();
		map = biz.detail(id, map);
		model.addAttribute("dataMap", map);
		model.addAttribute("msg", "");
		return "doctorEdit";
	}

	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "file") MultipartFile file, @RequestParam("id") int id, @ModelAttribute Doctor doctor, BindingResult bindingResult, Model model) {
		String path = "";
		if (!file.isEmpty()) {
			path = UploadUtil.uploadYa(uploadPath, request, file);
		}
		int count = biz.update(doctor, path);
		if (count > 0) {
			return "redirect:/doctor/query";
		} else {
			Map<String, Object> map = new HashMap<>();
			map = biz.detail(id, map);
			model.addAttribute("dataMap", map);
			model.addAttribute("msg", "修改失败");
			return "doctorEdit";
		}
	}
}
