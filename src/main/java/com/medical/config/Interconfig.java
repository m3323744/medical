package com.medical.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Component
public class Interconfig extends WebMvcConfigurerAdapter {
	
	public void  addInterceptors(InterceptorRegistry registry){
	    InterceptorRegistration addInterceptor = registry.addInterceptor(new LoginInterceptor());

	    addInterceptor.excludePathPatterns("/error");
	    addInterceptor.excludePathPatterns("/");
	    addInterceptor.excludePathPatterns("/admin/login");
	    addInterceptor.excludePathPatterns("/api/**");
	    addInterceptor.addPathPatterns("/**");
	}
}
