package com.medical.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.UserReceive;
import com.medical.service.UserReceiveService;

@RestController
@RequestMapping("/api/userReceive")
public class UserReceiveApiControlle {
	Logger logger = LoggerFactory.getLogger(UserReceiveApiControlle.class);

	@Autowired
	private UserReceiveService userReceiveService;

	@RequestMapping("/add")
	public ResultBean<UserReceive> add(@RequestParam(value = "userId", required = true) int userId) {
		ResultBean<UserReceive> bean = userReceiveService.add(userId);
		return bean;
	}

}
