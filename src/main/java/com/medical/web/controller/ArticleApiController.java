package com.medical.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Article;
import com.medical.entity.ArticleType;
import com.medical.entity.ResultBean;
import com.medical.service.ArticleService;

@RestController
@RequestMapping("/api/artic")
public class ArticleApiController {
	@Autowired
	private ArticleService biz;

	@RequestMapping("/getType")
	public ResultBean<List<ArticleType>> getType() {
		ResultBean<List<ArticleType>> bean = biz.getType();
		return bean;
	}

	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam(value = "typeId", defaultValue = "0") int typeId, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		Map<String, Object> map = new HashMap<>();
		map = biz.getList(typeId, pageIndex, map);
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>(map);
		return bean;
	}

	@RequestMapping("/getByKeyWord")
	public ResultBean<List<Article>> getByKeyWord(@RequestParam("keyWord") String keyWord) {
		return biz.getByKeyWord(keyWord);
	}

	@RequestMapping("/artcDateils")
	public ResultBean<Article> artcDateils(@RequestParam("id") int id) {
		Article art = biz.getDateils(id);
		ResultBean<Article> bean = new ResultBean<Article>(art);
		return bean;
	}
}
