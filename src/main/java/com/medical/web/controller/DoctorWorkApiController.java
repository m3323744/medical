package com.medical.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.DoctorWork;
import com.medical.entity.ResultBean;
import com.medical.service.DoctorWorkService;
import com.medical.util.DataUtil;

@RestController
@RequestMapping("/api/doctorwork")
public class DoctorWorkApiController {
	@Autowired
	private DoctorWorkService biz;
	
	@RequestMapping("/addWork")
	public ResultBean<DoctorWork> addWork(@ModelAttribute DoctorWork dw,@RequestParam("type")String type,@RequestParam("startTime")String startTime,@RequestParam("time")String time){
		dw.setBeginTime(DataUtil.strToDate(startTime));
		dw.setEndTime(DataUtil.strToDate(time));
		ResultBean<DoctorWork> bean=biz.addWork(dw,type);
		return bean;
	}
}
