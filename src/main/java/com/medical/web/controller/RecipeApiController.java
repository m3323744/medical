package com.medical.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.RecipeService;

@RestController
@RequestMapping("/api/recipe")
public class RecipeApiController {
	Logger logger = LoggerFactory.getLogger(RecipeApiController.class);

	@Autowired
	private RecipeService recipeService;

	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex, @RequestParam(value = "illnessTypeId", required = false) Integer illnessTypeId, @RequestParam(value = "name", required = false) String name) {
		Map<String, Object> map = new HashMap<>();
		map = recipeService.getList(pageIndex, illnessTypeId, name, map);
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>(map);
		return bean;
	}

	@RequestMapping("/getDetail")
	public ResultBean<Map<String, Object>> getDetail(@RequestParam(value = "id", required = true) int id) {
		ResultBean<Map<String, Object>> bean = recipeService.getDetail(id);
		return bean;
	}

}
