package com.medical.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.medical.entity.ResultBean;
import com.medical.entity.User;
import com.medical.service.UserService;
import com.medical.util.UploadUtil;

@RestController
@RequestMapping("/api/user")
public class UserApiController {
	@Autowired
	private UserService biz;

	@Value("${web.upload-path}")
	private String uploadPath;

	@RequestMapping("/register")
	public ResultBean<User> register(@ModelAttribute User user) {
		User use = biz.addwxUser(user);
		ResultBean<User> bean = new ResultBean<User>(use);
		return bean;
	}

	@RequestMapping("/getDetails")
	public ResultBean<User> getDetails(@RequestParam("id") int id) {
		return biz.getApiDateils(id);
	}

	@RequestMapping("/updateDetails")
	public ResultBean<Integer> updateDetails(@ModelAttribute User user) {
		return biz.updateApiDateils(user);
	}

	@RequestMapping("/updateHeadurl")
	public String updateHeadurl(@RequestParam(value = "file", required = true) MultipartFile file, HttpServletRequest request) {
		try {
			String url = "http://localhost:8080/" + UploadUtil.uploadYa(uploadPath, request, file);
			return url;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping("/updateName")
	public ResultBean<Integer> updateName(String name, String headurl, int id) {
		return biz.updateName(name, headurl, id);
	}
	
	@RequestMapping("/updatePhone")
	public ResultBean<Integer> updatePhone(String phone,int id){
		return biz.updatePhone(phone, id);
	}
}
