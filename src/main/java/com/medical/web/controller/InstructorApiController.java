package com.medical.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Instructor;
import com.medical.entity.ResultBean;
import com.medical.service.InstructorService;

@RestController
@RequestMapping("/api/instructor")
public class InstructorApiController {

	Logger logger = LoggerFactory.getLogger(InstructorApiController.class);

	@Autowired
	private InstructorService instructorService;

	@RequestMapping("/getCombox")
	public ResultBean<List<Instructor>> getCombox(@RequestParam("serviceCenterId") int serviceId) {
		ResultBean<List<Instructor>> bean = instructorService.getCombox(serviceId);
		return bean;
	}

}
