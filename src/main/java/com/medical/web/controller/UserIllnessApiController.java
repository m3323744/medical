package com.medical.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.UserIllness;
import com.medical.service.UserIllnessService;

@RestController
@RequestMapping("/api/userIllness")
public class UserIllnessApiController {
	@Autowired
	private UserIllnessService biz;
	
	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam("userId")int userId,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex){
		return biz.getList(userId, pageIndex);
	}
	
	@RequestMapping("/addIllness")
	public ResultBean<Integer> addIllness(@ModelAttribute UserIllness illness,@RequestParam("type")String type){
		return biz.addIllness(illness, type);
	}
}
