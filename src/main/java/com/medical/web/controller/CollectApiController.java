package com.medical.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.entity.UerCollect;
import com.medical.service.UserCollectService;

@RestController
@RequestMapping("/api/coll")
public class CollectApiController {
	@Autowired
	private UserCollectService biz;
	
	@RequestMapping("/addColl")
	public ResultBean<Integer> addColl(@ModelAttribute UerCollect coll){
		return biz.addCollect(coll);
	}
	
	@RequestMapping("/checkCollect")
	public ResultBean<Integer> checkCollect(@ModelAttribute UerCollect coll){
		return biz.checkCollect(coll);
	}
	
	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam("userId")int userId,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex){
		return biz.getList(userId, pageIndex);
	}
}
