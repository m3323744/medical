package com.medical.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Img;
import com.medical.entity.ResultBean;
import com.medical.service.ImgService;

@RestController
@RequestMapping("/api/img")
public class ImgApiController {
	Logger logger = LoggerFactory.getLogger(ImgApiController.class);

	@Autowired
	private ImgService imgService;

	@RequestMapping("/getList")
	public ResultBean<List<Img>> getList() {
		ResultBean<List<Img>> bean = imgService.getList();
		return bean;
	}

}
