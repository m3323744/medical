package com.medical.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.UserRecipeService;

@RestController
@RequestMapping("/api/userRecipe")
public class UserRecipeApiController {
	Logger logger = LoggerFactory.getLogger(UserRecipeApiController.class);

	@Autowired
	private UserRecipeService userRecipeService;

	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam(value = "userId", required = true) int userId, @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		Map<String, Object> map = new HashMap<>();
		map = userRecipeService.getList(userId, pageIndex, map);
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>(map);
		return bean;
	}

	@RequestMapping("/getCount")
	public ResultBean<Integer> getCount(@RequestParam(value = "userId", required = true) int userId) {
		int count = userRecipeService.getCount(userId);
		ResultBean<Integer> bean = new ResultBean<Integer>(count);
		return bean;
	}

}
