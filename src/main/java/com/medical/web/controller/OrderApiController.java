package com.medical.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.OrderService;

@RestController
@RequestMapping("/api/order")
public class OrderApiController {
	@Autowired
	private OrderService biz;
	
	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam("latitude")Double latitude,@RequestParam("longitude")Double longitude,@RequestParam("userId")int userId,@RequestParam(value="pageIndex",defaultValue="1")int pageIndex){
		return biz.getList(userId, pageIndex,latitude,longitude);
	}
}
