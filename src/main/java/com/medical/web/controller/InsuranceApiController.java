package com.medical.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Insurance;
import com.medical.entity.ResultBean;
import com.medical.service.InsuranceService;

@RestController
@RequestMapping("/api/insurance")
public class InsuranceApiController {
	Logger logger = LoggerFactory.getLogger(InsuranceApiController.class);

	@Autowired
	private InsuranceService insuranceService;

	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {
		Map<String, Object> map = new HashMap<>();
		map = insuranceService.getList(pageIndex, map);
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>(map);
		return bean;
	}

	@RequestMapping("/getDetail")
	public ResultBean<Insurance> getDetail(@RequestParam(value = "id", required = true) int id) {
		ResultBean<Insurance> bean = insuranceService.getDetail(id);
		return bean;
	}

}
