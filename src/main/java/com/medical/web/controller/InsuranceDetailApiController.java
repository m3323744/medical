package com.medical.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.InsuranceDetail;
import com.medical.entity.ResultBean;
import com.medical.service.InsuranceDetailService;

@RestController
@RequestMapping("/api/insuranceDetail")
public class InsuranceDetailApiController {
	Logger logger = LoggerFactory.getLogger(InsuranceDetailApiController.class);

	@Autowired
	private InsuranceDetailService insuranceDetailService;

	@RequestMapping("/getDetail")
	public ResultBean<InsuranceDetail> getDetail(@RequestParam(value = "type", required = true) int type) {
		ResultBean<InsuranceDetail> bean = insuranceDetailService.getDetail(type);
		return bean;
	}

}
