package com.medical.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Doctor;
import com.medical.entity.ResultBean;
import com.medical.service.DoctorService;

@RestController
@RequestMapping("/api/doctor")
public class DoctorApiController {

	Logger logger = LoggerFactory.getLogger(RecipeApiController.class);

	@Autowired
	private DoctorService doctorService;

	@RequestMapping("/addDoctor")
	public ResultBean<Doctor> addDoctor(@RequestParam("illness") String illness, @RequestParam("typeName") String typeName, @RequestParam("workId") String workId, @ModelAttribute Doctor doc) {
		ResultBean<Doctor> bean = doctorService.addDoctor(illness, typeName, workId, doc);
		return bean;
	}

	@RequestMapping("/isExist")
	public ResultBean<Map<String, Object>> isExist(@RequestParam(value = "userId") int userId) {
		ResultBean<Map<String, Object>> bean = doctorService.isExist(userId);
		return bean;
	}

	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex, @RequestParam(value = "illnessTypeId",  defaultValue = "0")int illnessTypeId, @RequestParam(value = "name", defaultValue = "") String name) {
		Map<String, Object> map = new HashMap<>();
		map = doctorService.getList(pageIndex, illnessTypeId, name, map);
		ResultBean<Map<String, Object>> bean = new ResultBean<Map<String, Object>>(map);
		return bean;
	}


	@RequestMapping("/getDetail")
	public ResultBean<Map<String, Object>> getDoctorDetail(@RequestParam(value = "id", required = true) int id) {
		ResultBean<Map<String, Object>> bean = doctorService.getDetail(id);
		return bean;
	}
	
	@RequestMapping("/getDoctorCombox")
	public ResultBean<List<Doctor>> getDoctorCombox() {
		ResultBean<List<Doctor>> bean = doctorService.getDoctorCombox();
		return bean;
	}

}
