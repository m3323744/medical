package com.medical.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.ResultBean;
import com.medical.service.ServiceCenterService;

@RestController
@RequestMapping("/api/serCenter")
public class ServiceCenterApiController {
	@Autowired
	private ServiceCenterService biz;

	@RequestMapping("/getList")
	public ResultBean<Map<String, Object>> getList(@RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude, @RequestParam(value = "name", defaultValue = "") String name) {
		return biz.getList(latitude, longitude, name);
	}
}
