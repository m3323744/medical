package com.medical.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.Chain;
import com.medical.entity.ResultBean;
import com.medical.service.ChainService;

@RestController
@RequestMapping("/api/chain")
public class ChainApiController {
	Logger logger = LoggerFactory.getLogger(ChainApiController.class);

	@Autowired
	private ChainService chainService;

	@RequestMapping("/getDetail")
	public ResultBean<Chain> getDetail() {
		ResultBean<Chain> bean = chainService.getDetail();
		return bean;
	}

}
