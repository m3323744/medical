package com.medical.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.DoctorType;
import com.medical.entity.ResultBean;
import com.medical.service.DoctorTypeService;

@RestController
@RequestMapping("/api/doctortype")
public class DoctorTypeApiController {
	@Autowired
	private DoctorTypeService biz;
	
	@RequestMapping("/getAll")
	public ResultBean<List<DoctorType>> getAll(){
		return biz.getAll();
	}
}
