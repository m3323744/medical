package com.medical.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medical.entity.IllnessType;
import com.medical.entity.ResultBean;
import com.medical.service.IllnessTypeService;

@RestController
@RequestMapping("/api/illnessType")
public class IllnessTypeApiController {
	Logger logger = LoggerFactory.getLogger(IllnessTypeApiController.class);

	@Autowired
	private IllnessTypeService illnessTypeService;

	@RequestMapping("/getType")
	public ResultBean<List<IllnessType>> getType() {
		ResultBean<List<IllnessType>> bean = illnessTypeService.getType();
		return bean;
	}

}
